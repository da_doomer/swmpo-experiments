from gymnasium.envs.registration import register
from swmpo_experiments.ground_truth_wrapper import GroundTruthWrapper
from gymnasium.envs.registration import WrapperSpec
from gymnasium.wrappers import TransformReward

register(
    id="BipedalWalkerHardcoreModesGroundTruth-v3",
    entry_point="bipedal_walker_hardcore_modes.env:BipedalWalkerModes",
    additional_wrappers=(
        WrapperSpec(
            name=GroundTruthWrapper.__name__,
            entry_point="swmpo_experiments.ground_truth_wrapper:GroundTruthWrapper",
            kwargs=dict(
                extrinsic_reward_scale=1.0,
                exploration_window_size=60,
                mode_n=5,
            ),
        ),
        WrapperSpec(
            name=TransformReward.__name__,
            entry_point="gymnasium.wrappers:TransformReward",
            kwargs=dict(
                f=lambda r: max(r, 0.0),
            ),
        ),
    ),
    kwargs={"hardcore": True},
    max_episode_steps=2000,
    reward_threshold=300,
)
