"""Generate and serialize trajectories."""
from swmpo_experiments.trajectory_dataset import serialize_trajectory
from swmpo.transition import Transition
from pathlib import Path
import concurrent.futures
import argparse
import random
from autonomous_car_verification.simulator.Car import World
from autonomous_car_verification.simulator.Car import THROTTLE
from autonomous_car_verification.simulator.plot_trajectories_7a import Modes
from autonomous_car_verification.simulator.plot_trajectories_7a import ComposedModePredictor
from autonomous_car_verification.simulator.plot_trajectories_7a import ComposedSteeringPredictor
from autonomous_car_verification.simulator.plot_trajectories_7a import reverse_lidar
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib as mpl
import inspect
import torch
import autonomous_car_verification


def generate_trajectory(
    dt: float,
    map_seed: str,
    traj_seed: str,
    task_id: int,
    output_dir: Path,
) -> None:
    """Generate trajectories to the given directory, which is assumed to
    exist.

    Returns a list of paths of the transitions, relative to the output
    directory; and a plot of the trajectory.
    """
    w = World(
        time_step=dt,
        map_seed=map_seed,
        initial_position_seed=traj_seed,
    )
    observation, _ = w.reset()

    # Load pretrained policy
    pretrained_dir = Path(
        inspect.getfile(autonomous_car_verification.simulator.Car)
    ).parent
    mode_predictor = ComposedModePredictor(
        pretrained_dir/'big.yml',
        pretrained_dir/'straight_little.yml',
        pretrained_dir/'square_right_little.yml',
        pretrained_dir/'square_left_little.yml',
        pretrained_dir/'sharp_right_little.yml',
        pretrained_dir/'sharp_left_little.yml',
        True,
    )
    action_scale = float(w.action_space.high[0])
    steering_ctrl = ComposedSteeringPredictor(
        pretrained_dir/'tanh64x64_right_turn_lidar.yml',
        pretrained_dir/'tanh64x64_sharp_turn_lidar.yml',
        action_scale,
    )

    # Ground mode truth mapping
    mode_map = {
        Modes.STRAIGHT: 0,
        Modes.SQUARE_RIGHT: 1,
        Modes.SQUARE_LEFT: 2,
        Modes.SHARP_RIGHT: 3,
        Modes.SHARP_LEFT: 4,
    }

    # Generate trajectory
    transitions = list[Transition]()
    ground_truth_states = list[int]()
    while True:
        source_state = torch.tensor(observation.tolist())

        # Predict policy mode
        mode = mode_predictor.predict(observation)
        if mode == Modes.SQUARE_LEFT or mode == Modes.SHARP_LEFT:
            observation = reverse_lidar(observation)

        # Call modular policy
        delta = steering_ctrl.predict(observation, mode)

        # Step environment
        observation, reward, terminated, truncated, info = w.step(
            delta,
            THROTTLE,
        )

        # Store transition
        next_state = torch.tensor(observation.tolist())
        action = torch.tensor(delta[0].tolist())
        transition = Transition(
            source_state=source_state,
            action=action,
            next_state=next_state,
        )
        transitions.append(transition)
        ground_truth_states.append(mode_map[mode])

        done = terminated or truncated
        if done:
            break

    # Predict policy mode for last observation
    mode = mode_predictor.predict(observation)
    ground_truth_states.append(mode_map[mode])

    # To simulate different trajectories, we simply select a sub-trajectory
    # from any state that has an initial state
    initial_mode = ground_truth_states[0]
    target_len = 300
    possible_starting_is = [
        i
        for i in range(len(ground_truth_states))
        if all((
            ground_truth_states[i] == initial_mode,
            i < len(ground_truth_states) - target_len,
        ))
    ]
    allX = w.allX
    allY = w.allY
    if len(possible_starting_is) > 0:
        _random = random.Random(traj_seed)
        start_i = _random.choice(possible_starting_is)
        end_i = start_i + target_len
        ground_truth_states = ground_truth_states[start_i:end_i]
        transitions = transitions[start_i:end_i]
        allX = allX[start_i:end_i]
        allY = allY[start_i:end_i]
    else:
        print(f"Warning: could not randomize trajectory")

    # Serialize trajectory
    serialize_trajectory(
        transitions=transitions,
        ground_truth_modes=ground_truth_states,
        task_id=task_id,
        output_dir=output_dir,
    )

    # Create matplotlib figure
    figure_path = output_dir/"trajectory.png"
    fig = Figure()
    _ = FigureCanvas(fig)
    ax = fig.add_subplot()
    w.plotHalls(ax=ax)
    cmap = mpl.colormaps['Pastel1']
    mode_cmap = cmap(
        list(mode_map.values())
    )
    mode_sequence = [
        mode_cmap[mode]
        for mode in ground_truth_states
    ]
    mode_sequence[0] = (0, 0, 0, 1)
    _ = ax.scatter(
        allX, allY, s=5, c=mode_sequence, label='straight'
    )
    fig.savefig(figure_path)
    print(f"Wrote {figure_path}")


def main(
    num_maps: int,
    num_traj_per_map: int,
    worker_n: int,
    output_dir: Path,
    dt: float,
    map_seed: str,
    trajectory_seed: str,
):
    _random_map = random.Random(map_seed)
    _random_traj = random.Random(trajectory_seed)

    # Create trajectory generation tasks
    tasks = list[tuple[Path, str, str]]()  # (traj_dir, map_seed, traj_seed)
    for map_i in range(num_maps):
        map_dir = output_dir / f"map_{map_i}"
        map_dir.mkdir()
        map_seed = str(_random_map.random())

        for traj_i in range(num_traj_per_map):
            traj_dir = map_dir / f"traj_{traj_i}"
            traj_dir.mkdir()
            traj_seed = str(_random_traj.random())

            task = (traj_dir, map_seed, traj_seed)
            tasks.append(task)

    # Gather trajectories
    with concurrent.futures.ProcessPoolExecutor(worker_n) as p:
        futures = list()
        for task_id, (traj_dir, map_seed, traj_seed) in enumerate(tasks):
            future = p.submit(
                generate_trajectory,
                output_dir=traj_dir,
                dt=dt,
                map_seed=map_seed,
                traj_seed=traj_seed,
                task_id=task_id,
            )
            futures.append(future)

        for future in futures:
            future.result()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='Generate trajectories',
        description='Generate and serialize trajectories.',
    )
    _ = parser.add_argument(
        '--output_dir',
        type=Path,
        required=True,
        help='Non-existing directory to write output files.'
    )
    parser.add_argument(
        '--num_maps',
        type=int,
        required=True,
        help='Number of maps to generate.'
    )
    parser.add_argument(
        '--num_traj_per_map',
        type=int,
        required=True,
        help='Number of trajectories per map to generate.'
    )
    _ = parser.add_argument(
        '--dt',
        type=float,
        required=True,
        help='Integration constant for the dynamical system.',
    )
    _ = parser.add_argument(
        '--worker_n',
        type=int,
        required=True,
        help='Number of workers to sample episodes.',
    )
    parser.add_argument(
        '--map_seed',
        type=str,
        required=True,
        help='Random number generator seed for map generation'
    )
    parser.add_argument(
        '--trajectory_seed',
        type=str,
        required=True,
        help='Random number generator seed for trajectory generation'
    )
    args = parser.parse_args()
    args.output_dir.mkdir()
    main(
        num_maps=args.num_maps,
        num_traj_per_map=args.num_traj_per_map,
        worker_n=args.worker_n,
        output_dir=args.output_dir,
        dt=args.dt,
        map_seed=args.map_seed,
        trajectory_seed=args.trajectory_seed,
    )
