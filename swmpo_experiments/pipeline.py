"""Run the experiment pipeline for a particular environment."""
import subprocess
from pathlib import Path
import hashlib
import datetime
import argparse
import json
import shutil
import random
import os
from swmpo_experiments.benchmark_partitions import plot_partition_errors
import concurrent.futures


def run_map_pipeline(
    output_dir: Path,
    target_map_id: int,
    state_machine_hidden_sizes: str,
    state_machine_learning_rate: float,
    state_machine_model_mode_iter_n: int,
    state_machine_partition_size: int,
    state_machine_min_island_size: int,
    state_machine_autoencoder_latent_size: int,
    state_machine_batch_size: int,
    state_machine_cluster_dimensionality_reduce: int,
    state_machine_information_content_regularization_scale: float,
    state_machine_mutual_information_regularization_scale: float,
    state_machine_mutual_information_mini_batch_size: int,
    state_machine_prunning_error_tolerance: float,
    state_machine_predicate_hyperparameters_path: Path,
    state_machine_local_model_hyperparameters_path: Path,
    train_trajectories_dir: Path,
    partition_benchmark_dir: Path,
    test_trajectories_dir: Path,
    src_dir: Path,
    worker_n: int,
    dt: float,
    seed: str,
    cuda_device: str,
):
    hmm_dir_global = output_dir / Path("hmm_global_" + str(target_map_id))
    slds_dir_global = output_dir / Path("slds_global_" + str(target_map_id))
    hmm_dir_local = output_dir / Path("hmm_local_" + str(target_map_id))
    slds_dir_local = output_dir / Path("slds_local_" + str(target_map_id))
    hmm_pkl_global = hmm_dir_global / "hmm.pkl"
    slds_pkl_global = slds_dir_global / "slds.pkl"
    hmm_pkl_local = hmm_dir_local / "hmm.pkl"
    slds_pkl_local = slds_dir_local / "slds.pkl"

    # Synthesize state machine
    print("Synthesizing state machine " +str(target_map_id) + "...")
    state_machine_dir = output_dir / Path("state_machine_" + str(target_map_id))
    subprocess.run([
        "python", str(src_dir / "swmpo_experiments/state_machine_synthesis.py"),
        "--output_dir", str(state_machine_dir),
        "--hidden_sizes", *state_machine_hidden_sizes.split(" "),
        "--learning_rate", str(state_machine_learning_rate),
        "--mode_model_iter_n", str(state_machine_model_mode_iter_n),
        "--state_n", str(state_machine_partition_size),
        "--min_island_size", str(state_machine_min_island_size),
        "--partition_latent_size", str(state_machine_autoencoder_latent_size),
        "--cluster_dimensionality_reduce", str(state_machine_cluster_dimensionality_reduce),
        "--cluster_information_content_regularization_scale", str(state_machine_information_content_regularization_scale),
        "--cluster_mutual_information_regularization_scale", str(state_machine_mutual_information_regularization_scale),
        "--cuda_device", cuda_device,
        "--batch_size", str(state_machine_batch_size),
        "--mutual_information_mini_batch_size", str(state_machine_mutual_information_mini_batch_size),
        "--prunning_error_tolerance", str(state_machine_prunning_error_tolerance),
        "--seed", seed,
        "--dt", str(dt),
        "--train_trajectory_dir", str(train_trajectories_dir),
        "--predicate_hyperparameters_json", str(state_machine_predicate_hyperparameters_path),
        "--local_model_hyperparameters_json", str(state_machine_local_model_hyperparameters_path),
        "--target_map_id", str(target_map_id)
    ])
    print("Synthesis of state machine finished.")

    # Inspect state machine
    print("Plotting state machine...")

    zip_file = output_dir / Path("state_machine_" + str(target_map_id)) / "state_machine.zip"
    prunned_zip_file = output_dir / Path("state_machine_" + str(target_map_id)) / "state_machine_prunned.zip"

    plot_dir = output_dir / Path("state_machine_plot_" + str(target_map_id))
    prunned_plot_dir = output_dir / Path("state_machine_plot_prunned_" + str(target_map_id))

    plot_tasks = [
        (plot_dir, zip_file),
        (prunned_plot_dir, prunned_zip_file)
    ]

    for plot_dir, zip_file in plot_tasks:
        subprocess.run([
            "python", str(src_dir / "swmpo_experiments/plot_state_machine.py"),
            "--output_dir", str(plot_dir),
            "--dt", str(dt),
            "--state_machine_zip", str(zip_file),
            "--test_trajectory_dir", str(test_trajectories_dir) + "/map_" + str(target_map_id)
        ])
    print("Plotting state machine done.")

    # Synthesize HMM
    print("Synthesizing global HMM...")
    subprocess.run([
        "python", str(src_dir / "swmpo_experiments/hmm_synthesis.py"),
        "--train_trajectory_dir", str(train_trajectories_dir),
        "--output_dir", str(hmm_dir_global),
        "--component_n", str(state_machine_partition_size),
        "--seed", seed,
        "--worker_n", str(worker_n)
    ])
    print("Synthesis of global HMM finished.")

    print("Synthesizing local HMM...")
    subprocess.run([
        "python", str(src_dir / "swmpo_experiments/hmm_synthesis.py"),
        "--train_trajectory_dir", str(train_trajectories_dir) + "/map_" + str(target_map_id),
        "--output_dir", str(hmm_dir_local),
        "--component_n", str(state_machine_partition_size),
        "--seed", seed,
        "--worker_n", str(worker_n)
    ])
    print("Synthesis of local HMM finished.")

    print("Synthesizing global SLDS...")
    subprocess.run([
        "python", str(src_dir / "swmpo_experiments/slds_synthesis.py"),
        "--train_trajectory_dir", str(train_trajectories_dir),
        "--output_dir", str(slds_dir_global),
        "--component_n", str(state_machine_partition_size),
        "--seed", seed,
        "--worker_n", str(worker_n),
        "--latent_dim", str(2)
    ])
    print("Synthesis of global SLDS finished.")

    print("Synthesizing local SLDS...")
    subprocess.run([
        "python", str(src_dir / "swmpo_experiments/slds_synthesis.py"),
        "--train_trajectory_dir", str(train_trajectories_dir) + "/map_" + str(target_map_id),
        "--output_dir", str(slds_dir_local),
        "--component_n", str(state_machine_partition_size),
        "--seed", seed,
        "--worker_n", str(worker_n),
        "--latent_dim", str(2)
    ])
    print("Synthesis of local SLDS finished.")

    # Compare HMM and state machine partition
    print("Benchmarking partition...")

    state_machine_dir = output_dir / Path("state_machine_" + str(target_map_id))
    state_machine_zip = state_machine_dir / "state_machine.zip"
    state_machine_prunned_zip = state_machine_dir / "state_machine_prunned.zip"

    subprocess.run([
        "python", str(src_dir / "swmpo_experiments/benchmark_partitions.py"),
        "--test_trajectory_dir", str(test_trajectories_dir) + "/map_" + str(target_map_id),
        "--output_dir", str(partition_benchmark_dir) + "_" + str(target_map_id),
        "--dt", str(dt),
        "--hmm_pkl_global", str(hmm_pkl_global),
        "--hmm_pkl_local", str(hmm_pkl_local),
        "--slds_pkl_global", str(slds_pkl_global),
        "--slds_pkl_local", str(slds_pkl_local),
        "--state_machine_zip", str(state_machine_zip),
    ])
    print("Partition benchmark done.")

    # Compare HMM and state machine forecasting
    #print("Benchmarking forecasting...")
    #subprocess.run([
    #    "python", str(src_dir / "swmpo_experiments/benchmark_forecasting.py"),
    #    "--test_trajectory_dir", str(test_trajectories_dir) + "/map_" + str(target_map_id),
    #    "--output_dir", str(forecast_benchmark_dir) + "_" + str(target_map_id),
    #    "--dt", str(dt),
    #    "--hmm_pkl_global", str(hmm_pkl_global),
    #    "--hmm_pkl_local", str(hmm_pkl_local),
    #    "--slds_pkl_global", str(slds_pkl_global),
    #    "--slds_pkl_local", str(slds_pkl_local),
    #    "--state_machine_zip", str(state_machine_zip),
    #])
    #print("Partition forecasting done.")


def main(
    seed: str,
    worker_n: int,
    cuda_device: str,

    # Environment hyperparameters
    dt: float,
    env_id: str,

    # State machine synthesis hyperparameters
    num_maps: int,
    num_traj_per_map: int,
    state_machine_hidden_sizes: str,
    state_machine_learning_rate: float,
    state_machine_partition_size: int,
    state_machine_batch_size: int,
    state_machine_min_island_size: int,
    state_machine_autoencoder_latent_size: int,
    state_machine_prunning_error_tolerance: float,
    state_machine_model_mode_iter_n: int,
    state_machine_cluster_dimensionality_reduce: int,
    state_machine_information_content_regularization_scale: float,
    state_machine_mutual_information_regularization_scale: float,
    state_machine_mutual_information_mini_batch_size: int,
    state_machine_predicate_hyperparameters: dict[str, str | int | float],
    state_machine_local_model_hyperparameters: dict[str, str | int | float],

    # Environment-specific hyperparameters
    environment_specific_hyperparameters: dict[str, str],

    # Shared results directory (assumed to exist). A subdir will be created
    results_dir: Path,

    # Cache dir
    trajectory_cache_dir: Path,
) -> Path:
    """Returns the directory with experiment results."""
    hyperparameters = {
        k: str(v)
        for k, v in locals().items()
    }

    _random = random.Random(seed)

    # Get a hash of the source code
    src_files = list(Path(".").rglob("*.py")) + list(Path(".").rglob("*.sh"))
    src_files = [
        f
        for f in src_files
        if not any(
            exclude in str(f) for exclude in ["venv", "output_results"]
        )
    ]
    experiment_hash = "".join(f.read_text() for f in src_files)
    experiment_id = hashlib.md5(experiment_hash.encode()).hexdigest()

    date_str = str(datetime.datetime.now().isoformat())
    _output_dir = f"{env_id}_output_results_{date_str}_{experiment_id}"
    output_dir = (results_dir / _output_dir).with_suffix("")
    output_dir.mkdir()
    output_zip = output_dir.with_suffix(".zip")

    train_trajectories_dir = output_dir / "train_trajectories"
    test_trajectories_dir = output_dir / "test_trajectories"
    state_machine_plot_dir = output_dir / "state_machine_plot"
    state_machine_prunned_plot_dir = output_dir / "state_machine_prunned_plot"
    date_file = output_dir / "date.txt"

    partition_benchmark_dir = output_dir / "partition_benchmark"
    forecast_benchmark_dir = output_dir / "forecast_benchmark"

    output_dir.mkdir(parents=True, exist_ok=True)
    print(f"Output dir: {output_dir}")
    print(f"Output zip: {output_zip}")

    # Serialize code
    src_dir = output_dir / "src"
    src_dir.mkdir()
    subprocess.run(["mkdir", str(src_dir / "lib")])
    subprocess.run(["cp", "-r", "lib/swmpo", str(src_dir / "lib")])
    # We don't copy rl-trained-agents because it's huge
    subprocess.run([
        "rsync", "-a", "--exclude", "rl-trained-agents", "lib", str(src_dir)
    ])
    subprocess.run(["cp", "-r", "swmpo_experiments", str(src_dir)])
    subprocess.run(["cp", "README.md", str(src_dir)])
    subprocess.run(["cp", "pyproject.toml", str(src_dir)])
    subprocess.run(["cp", "poetry.lock", str(src_dir)])

    # Serialize hyperparameters
    with open(output_dir / "hyperparameters.json", "wt") as fp:
        json.dump(hyperparameters, fp, indent=2)

    # Log date
    with open(date_file, "w") as f:
        f.write(datetime.datetime.now().isoformat())

    # Gather trajectories
    # Note: we use sys.executable to inherit the virtual virtual environment
    print("Gathering trajectories")
    trajectory_dirs = [train_trajectories_dir, test_trajectories_dir]

    _cached_dataset_dir = f"{num_maps}_maps_with_{num_traj_per_map}_trajectories"
    cached_dataset_dir = trajectory_cache_dir / env_id / _cached_dataset_dir
    map_seed = str(_random.random())
    for trajectory_dir in trajectory_dirs:
        tcache_dir = cached_dataset_dir / trajectory_dir.stem
        if not tcache_dir.exists():
            # Generate new trajectories to the cache dir
            print(f"Generating new cache {tcache_dir}")
            from swmpo_experiments.generate_trajectories import generate_trajectories
            cached_dataset_dir.mkdir(exist_ok=True, parents=True)
            generate_trajectories(
                env_id=env_id,
                src_dir=src_dir,
                trajectory_dir=tcache_dir,
                num_maps=num_maps,
                num_traj_per_map=num_traj_per_map,
                worker_n=worker_n,
                environment_specific_hyperparameters=environment_specific_hyperparameters,
                map_seed=map_seed,
                trajectory_seed=str(_random.random()),
                dt=dt,
            )

        # Cache directory
        print(f"Using cached trajectories {tcache_dir}")
        shutil.copytree(tcache_dir, trajectory_dir)
    print("Done")

    # Serialize predicate and local model hyperparameters for
    # state machine synthesis script
    state_machine_predicate_hyperparameters_path = output_dir/"predicate_hyperparameters.json"
    with open(state_machine_predicate_hyperparameters_path, "wt") as fp:
        json.dump(state_machine_predicate_hyperparameters, fp)

    state_machine_local_model_hyperparameters_path = output_dir/"local_model_hyperparameters.json"
    with open(state_machine_local_model_hyperparameters_path, "wt") as fp:
        json.dump(state_machine_local_model_hyperparameters, fp)

    with concurrent.futures.ProcessPoolExecutor(worker_n) as p:
        futures = list()
        for target_map_id in range(num_maps):
            future = p.submit(
                run_map_pipeline,
                output_dir=output_dir,
                target_map_id=target_map_id,
                state_machine_hidden_sizes=state_machine_hidden_sizes,
                state_machine_learning_rate=state_machine_learning_rate,
                state_machine_model_mode_iter_n=state_machine_model_mode_iter_n,
                state_machine_partition_size=state_machine_partition_size,
                state_machine_min_island_size=state_machine_min_island_size,
                state_machine_autoencoder_latent_size=state_machine_autoencoder_latent_size,
                state_machine_batch_size=state_machine_batch_size,
                state_machine_cluster_dimensionality_reduce=state_machine_cluster_dimensionality_reduce,
                state_machine_information_content_regularization_scale=state_machine_information_content_regularization_scale,
                state_machine_mutual_information_regularization_scale=state_machine_mutual_information_regularization_scale,
                state_machine_mutual_information_mini_batch_size=state_machine_mutual_information_mini_batch_size,
                state_machine_prunning_error_tolerance=state_machine_prunning_error_tolerance,
                state_machine_predicate_hyperparameters_path=state_machine_predicate_hyperparameters_path,
                state_machine_local_model_hyperparameters_path=state_machine_local_model_hyperparameters_path,
                train_trajectories_dir=train_trajectories_dir,
                test_trajectories_dir=test_trajectories_dir,
                partition_benchmark_dir=partition_benchmark_dir,
                src_dir=src_dir,
                worker_n=worker_n,
                dt=dt,
                seed=seed,
                cuda_device=cuda_device,
            )
            futures.append(future)
        for future in futures:
            future.result()

    # Set base directory and number of target ids
    base_dir = output_dir / Path("partition_benchmark_aggregate")

    # Ensure aggregate directory exists
    os.makedirs(base_dir, exist_ok=True)

    # Dictionary to store aggregated errors
    aggregated_errors = {}

    # Iterate over subdirectories
    for i in range(num_maps):
        subdir = output_dir / Path("partition_benchmark_" + str(i))
        json_path = os.path.join(subdir, "visited_states_errors.json")

        print(json_path)

        # Check if JSON file exists
        if os.path.exists(json_path):
            with open(json_path, "r") as f:
                data = json.load(f)

            # Aggregate the error values by concatenation
            for key, values in data.items():
                if key not in aggregated_errors:
                    aggregated_errors[key] = []
                aggregated_errors[key].extend(values)

    # Save aggregated errors to a JSON file
    aggregate_json_path = os.path.join(base_dir, "aggregated_errors.json")
    with open(aggregate_json_path, "w") as f:
        json.dump(aggregated_errors, f, indent=4)

    print(f"Aggregated errors saved to {aggregate_json_path}")

    # Generate error bar plot

    errors_plot_path = base_dir / "errors.svg"
    plot_partition_errors(
        errors=aggregated_errors,
        output_path=errors_plot_path,
    )

    print("Pipeline complete!")
    subprocess.run(["zip", "-r", "-q", str(output_zip), str(output_dir)])
    print(f"Wrote {output_zip}")

    return output_dir


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='Experiment pipeline',
        description='Execute the full experiment pipeline.',
    )
    parser.add_argument(
        '--hyperparameter_json',
        type=Path,
        required=True,
        help='JSON file with hyperparameters.'
    )
    args = parser.parse_args()

    # Create results dir if it doesn't exist
    results_dir = Path("output_results")
    results_dir.mkdir(exist_ok=True)
    trajectory_cache_dir = results_dir / "trajectory_cache"
    trajectory_cache_dir.mkdir(exist_ok=True)

    # Parse json
    with open(args.hyperparameter_json, "rt") as fp:
        hyperparameters = json.load(fp)

    main(
        **hyperparameters,
        results_dir=results_dir,
        trajectory_cache_dir=trajectory_cache_dir,
    )
