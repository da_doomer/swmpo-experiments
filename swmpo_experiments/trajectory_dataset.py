"""Trajectory datasets.


Datasets are expected to have the following structure:

```
dataset_dir/
    0/
        trajectory.json
        0.zip
        1.zip
        ...
    1/
        ...
    ...
```

With `episode.json` with the following structure:

```.json
{
    "transitions": ["0.zip", "1.zip", ...],
    "ground_truth_modes": [0, 0, 1, ...],
    "traj_id": 7,
}
```

Each transition is assumed to be serialized as a `float32` tensor."""
from swmpo.transition import deserialize
from swmpo.transition import Transition
import json
from dataclasses import dataclass
from pathlib import Path
from swmpo.transition import serialize


@dataclass
class Dataset:
    """An episode is a sequence of transitions. Each transition
    is associated with a ground truth mode.

    A task is the "environment" in which the episode was generated. For
    example, it might correspond to a terrain configuration.

    Each episode is associated with a single task. A task might
    have multiple episodes associated.
    """
    episodes: list[list[Transition]]
    ground_truth_modes: list[list[int]]
    map_ids: list[int]
    traj_ids: list[int]



def deserialize_dataset(dataset_dir: Path, target_map_id = None) -> Dataset:
    episodes = list[list[Transition]]()
    ground_truths = list[list[int]]()
    map_ids = list[int]()
    traj_ids = list[int]()

    for trajectory_json_path in sorted(dataset_dir.glob("**/trajectory.json")):
        with open(trajectory_json_path, "rt") as fp:
            trajectory_json = json.load(fp)

        transition_paths: list[str] = trajectory_json["transitions"]
        ground_truth_modes: list[int] = trajectory_json["ground_truth_modes"]
        traj_id: int = trajectory_json["traj_id"]
        map_id: int = int(trajectory_json_path.parent.name[-1])
        trajectory_dir = trajectory_json_path.parent
        if target_map_id is not None and map_id != target_map_id:
            continue
        episode = [
            deserialize(trajectory_dir/transition_path)
            for transition_path in transition_paths
        ]

        if ground_truth_modes[0] != 0:
            print(f"WARNING: ignoring trajectory '{trajectory_json_path}' because initial mode is not 0, its {ground_truth_modes[0]}!")
            continue

        traj_ids.append(traj_id)
        map_ids.append(traj_id)
        ground_truths.append(ground_truth_modes)
        episodes.append(episode)

    dataset = Dataset(
        episodes=episodes,
        ground_truth_modes=ground_truths,
        map_ids=map_ids,
        traj_ids=traj_ids
    )
    return dataset


def serialize_trajectory(
    transitions: list[Transition],
    ground_truth_modes: list[int],
    task_id: int,
    output_dir: Path,
):
    """`output_dir` is assumed to exist."""
    # Serialize each transition
    transition_paths = [
        (f"{i}.zip").rjust(10, "0")
        for i in range(len(transitions))
    ]
    for transition, transition_path in zip(transitions, transition_paths):
        serialize(transition, output_dir/transition_path)

    # Construct trajectory JSON
    trajectory_json = dict(
        transitions=transition_paths,
        ground_truth_modes=ground_truth_modes,
        traj_id=task_id,
    )

    # Serialize trajectory metadata
    trajectory_json_path = output_dir/"trajectory.json"
    with open(trajectory_json_path, "wt") as fp:
        json.dump(trajectory_json, fp)
    print(f"Wrote {trajectory_json_path}")
