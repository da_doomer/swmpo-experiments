"""Compare different models."""
from pathlib import Path
from swmpo_experiments.terrain_mass_utils.collect_data import sample_task_episode
from terrain_mass.task import get_example_task
from terrain_mass.task import Task
import argparse
import random
import concurrent.futures
import multiprocessing as mp


def main(
    output_dir: Path,
    num_maps: int,
    num_traj_per_map: int,
    worker_n: int,
    mpc_iter_n: int,
    mpc_plan_len: int,
    simulation_step_n: int,
    mpc_initial_stdev: float,
    map_seed: str,
    trajectory_seed: str,
    animation_fps: int,
):
    # Define environment
    _random_map = random.Random(map_seed)
    _random_traj = random.Random(trajectory_seed)

    # Define trajectory generation specifications (aka tasks. Task=
    # a specific trajectory in a specific map).
    tasks = list[tuple[Path, Task]]()
    for map_i in range(num_maps):
        map_dir = output_dir / f"map_{map_i}"
        map_dir.mkdir()
        map_seed = str(_random_map.random())

        for map_trajectory_j in range(num_traj_per_map):
            trajectory_dir = map_dir / f"trajectory_{map_trajectory_j}"
            trajectory_dir.mkdir()
            trajectory_seed = str(_random_traj.random())

            task = get_example_task(
                map_seed=map_seed,
                waypoint_seed=trajectory_seed,
            )

            spec = (trajectory_dir, task)
            tasks.append(spec)

    # Generate trajectories in parallel
    print("Collecting data...")
    ctx = mp.get_context('spawn')
    with concurrent.futures.ProcessPoolExecutor(worker_n, mp_context=ctx) as p:
        futures = list()
        for i, (task_dir, task) in enumerate(tasks):
            future = p.submit(
                sample_task_episode,
                task=task,
                task_id=i,
                mpc_iter_n=mpc_iter_n,
                mpc_plan_len=mpc_plan_len,
                mpc_initial_stdev=mpc_initial_stdev,
                simulation_step_n=simulation_step_n,
                animation_fps=animation_fps,
                output_dir=task_dir,
                seed=str(_random_traj.random()),
            )
            futures.append(future)

        for _, future in enumerate(futures):
            future.result()
            print(f"Collecting data ({_+1}/{len(tasks)})...")
    print("Done.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='Generate trajectories.',
        description='Generate a dataset of trajectories from the terrain mass environment using an expert policy.',
    )
    parser.add_argument(
        '--output_dir',
        type=Path,
        required=True,
        help='Non-existing directory to write output files'
    )
    parser.add_argument(
        '--num_maps',
        type=int,
        required=True,
        help='Number of maps'
    )
    parser.add_argument(
        '--num_traj_per_map',
        type=int,
        required=True,
        help='Number of trajectories per map'
    )
    parser.add_argument(
        '--worker_n',
        type=int,
        required=True,
        help='Number of workers to sample episodes',
    )
    parser.add_argument(
        '--map_seed',
        type=str,
        required=True,
        help='Random number generator seed for map generation'
    )
    parser.add_argument(
        '--trajectory_seed',
        type=str,
        required=True,
        help='Random number generator seed for trajectory generation'
    )
    parser.add_argument(
        '--mpc_plan_len',
        type=int,
        required=True,
        help='Length of the MPC local plan.',
    )
    parser.add_argument(
        '--mpc_initial_stdev',
        type=float,
        required=True,
        help='Standard deviation for the MPC optimization algorithm.',
    )
    parser.add_argument(
        '--mpc_iter_n',
        type=int,
        required=True,
        help='Maximum number of iterations for the MPC loop.'
    )
    parser.add_argument(
        '--simulation_step_n',
        type=int,
        required=True,
        help='Maximum number of steps for the simulations.'
    )
    args = parser.parse_args()
    args.output_dir.mkdir()
    main(
        output_dir=args.output_dir,
        num_maps=args.num_maps,
        num_traj_per_map=args.num_traj_per_map,
        map_seed=args.map_seed,
        trajectory_seed=args.trajectory_seed,
        worker_n=args.worker_n,
        mpc_iter_n=args.mpc_iter_n,
        mpc_plan_len=args.mpc_plan_len,
        mpc_initial_stdev=args.mpc_initial_stdev,
        simulation_step_n=args.simulation_step_n,
        animation_fps=60,
    )
