"""Collect data using a controller."""
from terrain_mass.task import Task
from terrain_mass.gymnasium import get_distance_to_target
from swmpo_experiments.terrain_mass_utils.mpc.planner import get_plan
from swmpo_experiments.terrain_mass_utils.ground_truth_states import get_ground_truth_states
from swmpo.transition import Transition
from swmpo_experiments.trajectory_dataset import serialize_trajectory
from terrain_mass.plotting import plot_animation
from dataclasses import dataclass
from pathlib import Path
import random
import torch
import concurrent.futures
import multiprocessing as mp


TARGET_POSITION_PATH = "target_position.json"
TRANSITION_PATHS = "transition_paths.json"
TASK_PATH = "task.pickle"


@dataclass
class TaskEpisode:
    episode: list[Transition]
    target_position: tuple[float, float]
    task: Task


def get_episode_transitions(
        task: Task,
        mpc_plan_len: int,
        mpc_iter_n: int,
        simulation_step_n: int,
        mpc_initial_stdev: float,
        seed: str,
        ) -> list[Transition]:
    """Return a list of transitions by calling an MPC control
    over a single episode."""
    _random = random.Random(seed)

    # Call MPC in a loop
    states = [task.environment.get_initial_state()]
    transitions = list[Transition]()

    # Assemble complete list of waypoints
    waypoints = task.waypoints + [task.target_position]
    current_waypoint_i = 0

    # Update MPC plan looping first action
    # (this induces a bias towards periodical motion
    # but it doesn't matter too much)
    for _ in range(simulation_step_n):
        # Initialize local plan
        plan = torch.tensor([
            (0.0, 0.0)
            for _ in range(mpc_plan_len)
        ])

        # Optimize local MPC plan
        initial_candidate_plan = torch.cat([
            plan[1:],
            plan[-1].unsqueeze(0),
        ])
        current_waypoint = waypoints[current_waypoint_i]
        plan = get_plan(
            initial_state=states[-1],
            initial_candidate_plan=initial_candidate_plan,
            environment=task.environment,
            iter_n=mpc_iter_n,
            target_position=current_waypoint,
            dt=task.dt,
            initial_stdev=mpc_initial_stdev,
            success_distance_to_target=task.success_distance_to_target,
            seed=str(_random.random()),
            action_min=task.environment.action_min,
            action_max=task.environment.action_max,
            verbose=False,
        )

        action = plan[0]

        # Step simulation
        source_state = states[-1]
        next_state = task.environment.step(
            x=source_state,
            action=action,
            dt=task.dt,
        )
        states.append(next_state)

        # Record transition
        transition = Transition(
            source_state=source_state,
            action=action,
            next_state=next_state,
        )
        transitions.append(transition)

        target_position_t = torch.tensor(current_waypoint)
        distance = get_distance_to_target(next_state, target_position_t)
        if distance < task.success_distance_to_target:
            current_waypoint_i += 1

        if current_waypoint_i >= len(waypoints):
            # This is true when we reach the target position after going
            # through all the intermediate waypoints
            break

    return transitions


def sample_task_episode(
    task: Task,
    task_id: str,
    mpc_initial_stdev: float,
    mpc_iter_n: int,
    mpc_plan_len: int,
    simulation_step_n: int,
    seed: str,
    animation_fps: int,
    output_dir: Path,
) -> None:
    """`output_dir` is assumed to exist."""
    episode = get_episode_transitions(
        task=task,
        mpc_iter_n=mpc_iter_n,
        mpc_plan_len=mpc_plan_len,
        simulation_step_n=simulation_step_n,
        mpc_initial_stdev=mpc_initial_stdev,
        seed=seed,
    )
    episode = TaskEpisode(
        episode=episode,
        target_position=task.target_position,
        task=task,
    )

    transitions = episode.episode

    ground_truth_states = get_ground_truth_states(
        environment_instance=episode.task.environment,
        episode=transitions,
    )

    # Serialize trajectory
    serialize_trajectory(
        transitions=transitions,
        ground_truth_modes=ground_truth_states,
        task_id=task_id,
        output_dir=output_dir,
    )

    # Plot trajectory animation
    episode_animation_path = output_dir/"trajectory.mp4"
    states = [
        transition.next_state
        for transition in transitions
    ]
    plot_animation(
        states,
        mass_radius=0.1,
        environment=episode.task.environment,
        fps=animation_fps,
        target_position=episode.task.target_position,
        output_path=episode_animation_path,
        target_position_radius=task.success_distance_to_target,
        waypoints=task.waypoints,
    )
    print(f"Wrote {episode_animation_path}")
