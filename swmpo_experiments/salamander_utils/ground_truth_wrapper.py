from swmpo_experiments.ground_truth_wrapper import GroundTruthWrapper
from gymnasium.envs.registration import register
from gymnasium.envs.registration import WrapperSpec

register(
    id="GroundTruthSalamander-v0",
    entry_point="salamander_env.salamander_gym:SalamanderEnv",
    additional_wrappers=(
        WrapperSpec(
            name=GroundTruthWrapper.__name__,
            entry_point="swmpo_experiments.ground_truth_wrapper:GroundTruthWrapper",
            kwargs=dict(
                extrinsic_reward_scale=1.0,
                mode_n=5,
                exploration_window_size=30,
            ),
        ),
    ),
    kwargs=dict(),
)
