"""Synthesize a Switching Linear Dynamical System (SLDS) using ssm."""
import os
os.environ['OPENBLAS_NUM_THREADS'] = '1'
from swmpo_experiments.visited_states_plotting import plot_visited_states
from swmpo_experiments.state_machine_synthesis import deserialize_dataset
from swmpo.transition import Transition
from swmpo.transition import deserialize
from swmpo.sequence_distance import get_best_permutation
from pathlib import Path
import argparse
import concurrent.futures
import json
import random
import pickle
import numpy as np
import ssm

# Avoid pytorch from doing threading. This is so that the script doesn't
# take over the computer's resources. You can remove these lines if not running
# on a lab computer.
import torch
torch.set_num_threads(1)

def get_transition_vector(t: Transition) -> np.ndarray:
    return np.concatenate([
        t.source_state,
        t.action,
        t.next_state,
    ])

def get_slds_visited_states(
        slds: ssm.SLDS,
        episode: list[Transition],
        ) -> list[int]:
    # Convert the episode into the correct data format
    X = np.array([
        get_transition_vector(t)
        for t in episode
    ])

    # Fit the SLDS model (if not already trained)
    #slds.initialize(X)
    q_lem_elbos, q_laplace_em = slds.approximate_posterior(
        X,
        method="laplace_em",
        variational_posterior="structured_meanfield",
        initialize=False,
        num_iters=50
    )

    # Extract the variational mean
    _, q_lem_x = q_laplace_em.mean[0]

    # Decode the discrete states
    z = slds.most_likely_states(q_lem_x, X)
    return z.tolist()


def plot_slds_episode(
        trajectory_dir: Path,
        slds_path: Path,
        output_dir: Path,
        ):
    # Deserialize episode trajectory
    episode_json = trajectory_dir / "transitions.json"
    with open(episode_json, "rt") as fp:
        transition_paths = json.load(fp)
    episode = [
        deserialize(trajectory_dir / transition_path)
        for transition_path in transition_paths
    ]

    # Deserialize model
    with open(slds_path, "rb") as file:
        slds: ssm.SLDS = pickle.load(file)

    # Deserialize ground truth states
    ground_truth_visited_states_path = trajectory_dir / "ground_truth_states.json"
    with open(ground_truth_visited_states_path, "rt") as fp:
        ground_truth_visited_states = json.load(fp)

    # Run SLDS
    visited_states = get_slds_visited_states(slds, episode)

    # Write states
    visited_states_path = output_dir / "visited_states.json"
    json_str = json.dumps(visited_states, indent=2)
    with open(visited_states_path, "wt") as fp:
        fp.write(json_str)
    print(f"Wrote {visited_states_path}")

    # Plot state machine error
    visited_states_plot_path = output_dir / "visited_states.svg"
    new_visited_states = get_best_permutation(
        visited_states,
        ground_truth_visited_states,
        initial_state=-1,  # SLDS is unconstrained to find best permutation
    )
    plot_visited_states(
        output_path=visited_states_plot_path,
        available_indices=set(new_visited_states) | set(ground_truth_visited_states),
        visited_states=dict(
            slds=new_visited_states,
            ground_truth=ground_truth_visited_states,
        ),
    )
    print(f"Wrote {visited_states_plot_path}")

def log_slds(
        slds: ssm.SLDS,
        episode_dir: Path,
        output_dir: Path,
        p: concurrent.futures.ProcessPoolExecutor,
        ):
    # Serialize model
    slds_path = output_dir / "slds.pkl"
    with open(slds_path, "wb") as file:
        pickle.dump(slds, file)
    print(f"Serialized SLDS to {slds_path}")

    # Serialize state machine states for each episode
    episodes_dir = output_dir / "input_episodes"
    episodes_dir.mkdir()
    futures = list()
    for i, trajectory_json in enumerate(episode_dir.glob("**/transitions.json")):
        trajectory_dir = trajectory_json.parent
        episode_output_dir = output_dir / f"{i}"
        episode_output_dir.mkdir()
        future = p.submit(
            plot_slds_episode,
            trajectory_dir=trajectory_dir,
            slds_path=slds_path,
            output_dir=episode_output_dir,
        )
        futures.append(future)

    for future in futures:
        future.result()

def main(
        episode_dir: Path,
        output_dir: Path,
        worker_n: int,
        component_n: int,
        latent_dim: int,
        seed: str,
        ):
    _random = random.Random(seed)

    # Deserialize dataset
    dataset = deserialize_dataset(episode_dir)
    episodes = dataset.episodes

    # Prepare data as a list of sequences
    data = [
        np.array([get_transition_vector(t) for t in episode])
        for episode in episodes
    ]

    # Synthesize SLDS using ssm
    slds = ssm.SLDS(
        N=data[0].shape[1],  # Dimensionality of observations
        K=component_n,       # Number of discrete states
        D=latent_dim,        # Dimensionality of continuous latent state
        dynamics="gaussian", # Type of continuous dynamics
        observations="gaussian",
        random_state=int.from_bytes(_random.randbytes(3), 'big', signed=False)
    )

    # Fit the SLDS to the data
    slds.fit(data, method="laplace_em", num_iters=50)

    # Log SLDS
    with concurrent.futures.ProcessPoolExecutor(worker_n) as p:
        log_slds(
            slds=slds,
            episode_dir=episode_dir,
            output_dir=output_dir,
            p=p,
        )

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='SLDS synthesis example',
        description='Synthesize a SLDS using ssm',
    )
    parser.add_argument(
        '--output_dir',
        type=Path,
        required=True,
        help='Non-existing directory to write output files'
    )
    parser.add_argument(
        '--component_n',
        type=int,
        required=True,
        help='Number of discrete states in the SLDS',
    )
    parser.add_argument(
        '--latent_dim',
        type=int,
        required=True,
        help='Dimensionality of the continuous latent state',
    )
    parser.add_argument(
        '--worker_n',
        type=int,
        required=True,
        help='Number of workers to process episodes',
    )
    parser.add_argument(
        '--seed',
        type=str,
        required=True,
        help='Random number generator seed.'
    )
    parser.add_argument(
        '--train_trajectory_dir',
        type=Path,
        required=True,
        help=(
            "Directory with transition dataset in the format described in "
            "this module's documentation"
        ),
    )
    args = parser.parse_args()
    args.output_dir.mkdir(exist_ok=True)
    main(
        episode_dir=args.train_trajectory_dir,
        output_dir=args.output_dir,
        component_n=args.component_n,
        latent_dim=args.latent_dim,
        seed=args.seed,
        worker_n=args.worker_n,
    )
