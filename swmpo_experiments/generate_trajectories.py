"""Generate trajectories from an environment."""
import subprocess
from pathlib import Path


def generate_trajectories(
    env_id: str,
    src_dir: Path,
    trajectory_dir: Path,
    num_maps: int,
    num_traj_per_map: int,
    worker_n: int,
    environment_specific_hyperparameters: dict[str, object],
    map_seed: str,
    trajectory_seed: str,
    dt: float,
):
    if env_id == "TerrainMass-v0":
        _ = subprocess.run([
            "python", str(src_dir / "swmpo_experiments" / "terrain_mass_utils" / "generate_trajectories.py"),
            "--output_dir", str(trajectory_dir),
            "--num_maps", str(num_maps),
            "--num_traj_per_map", str(num_traj_per_map),
            "--worker_n", str(worker_n),
            "--map_seed", map_seed,
            "--trajectory_seed", trajectory_seed,
            "--mpc_plan_len", str(environment_specific_hyperparameters["mpc_plan_len"]),
            "--mpc_initial_stdev", str(environment_specific_hyperparameters["mpc_initial_stdev"]),
            "--mpc_iter_n", str(environment_specific_hyperparameters["mpc_iter_n"]),
            "--simulation_step_n", str(environment_specific_hyperparameters["simulation_step_n"])
        ])
    elif env_id == "AutonomousCar-v0":
        _ = subprocess.run([
            "python", str(src_dir / "swmpo_experiments" / "autonomous_driving_utils" / "generate_trajectories.py"),
            "--output_dir", str(trajectory_dir),
            "--num_maps", str(num_maps),
            "--num_traj_per_map", str(num_traj_per_map),
            "--dt", str(dt),
            "--worker_n", str(worker_n),
            "--map_seed", map_seed,
            "--trajectory_seed", trajectory_seed,
        ])
    elif env_id == "Salamander-v0":
        _ = subprocess.run([
            "python", str(src_dir / "swmpo_experiments" / "salamander_utils" / "generate_trajectories.py"),
            "--output_dir", str(trajectory_dir),
            "--num_maps", str(num_maps),
            "--num_traj_per_map", str(num_traj_per_map),
            "--worker_n", str(worker_n),
            "--map_seed", map_seed,
            "--trajectory_seed", trajectory_seed,
        ])
    elif env_id == "BipedalWalkerHardcoreModes-v3":
        EXPERT_POLICY_DIR = Path(__file__).parent.parent/"lib"/"rl-trained-agents"/"sac"/"BipedalWalkerHardcore-v3_1"
        print(EXPERT_POLICY_DIR.resolve())
        assert EXPERT_POLICY_DIR.exists()
        _ = subprocess.run([
            "python", str(src_dir / "swmpo_experiments" / "bipedal_walker_hardcore_utils" / "generate_trajectories.py"),
            "--output_dir", str(trajectory_dir),
            "--num_maps", str(num_maps),
            "--num_traj_per_map", str(num_traj_per_map),
            "--worker_n", str(worker_n),
            "--expert_policy_dir", str(EXPERT_POLICY_DIR),
            "--map_seed", map_seed,
            "--trajectory_seed", trajectory_seed,
        ])
    else:
        raise ValueError(f"Environment {env_id} not recognized!")
