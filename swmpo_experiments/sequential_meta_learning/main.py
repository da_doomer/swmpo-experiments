"""Run the "mode guidance" vs "no mode guidance" vs "SWMPO" RL experiment."""
from pathlib import Path
from typing import Any
import subprocess
import hashlib
import datetime
import sys
import argparse
import json
import shutil
import threading
import time


def wait_for_jobs(max_jobs: int, jobs: list[subprocess.Popen]):
    while len(jobs) > max_jobs:
        j = jobs.pop()
        j.wait()


def async_submit_job(args: list[str]):
    j = subprocess.Popen(args)
    return j


def plot_rewards(
    swmpo_dir: Path,
    rl_dir: Path,
    model_based_rl_dir: Path,
    src_dir: Path,
    output_dir: Path,
):
    """Output_dir is assumed to exist. A sub-directory with the current
    timestamp will be created."""
    date_str = str(datetime.datetime.now().isoformat())
    new_dir = output_dir / f"plots_{date_str}"
    new_dir.mkdir()
    plot_file = new_dir / "rewards.svg"
    tasks_costs_plot_file = new_dir / "task_costs.svg"

    # Plot rewards
    swmpo_csvs = list(Path(swmpo_dir).rglob("*.csv"))
    rl_csvs = list(Path(rl_dir).rglob("*.csv"))
    model_based_rl_csvs = list(Path(model_based_rl_dir).rglob("*.csv"))
    subprocess.run([
        sys.executable, str(src_dir / "swmpo_experiments" / "reward_plotting.py"),
        "--rl_csvs", *[str(csv) for csv in rl_csvs],
        "--swmpo_csvs", *[str(csv) for csv in swmpo_csvs],
        "--model_based_rl_csvs", *[str(csv) for csv in model_based_rl_csvs],
        "--output_path", str(plot_file)
    ])

    # Plot costs
    swmpo_task_cost_jsons = list(Path(swmpo_dir).rglob("**/steps_required_per_task.json"))
    rl_task_cost_jsons = list(Path(rl_dir).rglob("**/steps_required_per_task.json"))
    model_based_rl_jsons = list(
        Path(model_based_rl_dir).rglob("**/steps_required_per_task.json")
    )
    subprocess.run([
        sys.executable, str(src_dir / "swmpo_experiments" / "task_cost_plotting.py"),
        "--rl_jsons", *[str(csv) for csv in rl_task_cost_jsons],
        "--swmpo_jsons", *[str(csv) for csv in swmpo_task_cost_jsons],
        "--model_based_rl_jsons", *[str(csv) for csv in model_based_rl_jsons],
        "--output_path", str(tasks_costs_plot_file)
    ])
    print(f"Wrote {new_dir}")


def plot_rewards_until_signal(
    stop_event,
    interval_seconds: float,
    swmpo_dir: Path,
    rl_dir: Path,
    model_based_rl_dir: Path,
    src_dir: Path,
    output_dir: Path,
):
    while not stop_event.is_set():  # Run until stop_event is set
        time.sleep(interval_seconds)
        print("Plotting...")
        try:
            plot_rewards(
                swmpo_dir=swmpo_dir,
                rl_dir=rl_dir,
                model_based_rl_dir=model_based_rl_dir,
                output_dir=output_dir,
                src_dir=src_dir,
            )
        except Exception as e:
            print(f"Error plotting!! {str(e)}")


def main(
    seed: str,
    worker_n: int,
    cuda_device: str,

    # RL hyperparameters
    rl_train_timestep_n: int,
    rl_datapoint_n: int,
    rl_experiment_n: int,
    rl_parallel_env_n: int,
    rl_plot_episode_n: int,
    rl_eval_episode_n: int,
    rl_session_train_steps: int,
    env_id: str,
    ground_truth_env_id: str,
    stack_observations_n: int,
    env_kwargs: dict[str, Any],

    # Model-based RL hyperparameters
    model_based_rl_session_train_steps: int,
    model_based_rl_trajectory_buffer_size: int,
    data_collection_step_n: int,

    # Model hyperparameters
    model_dt: float,
    model_episode_len: int,
    model_hyperparameters: dict[str, float | int | str],

    # SWMPO hyperparameters
    partition_zip: Path,
    partition_min_island_size: int,
    partition_prunning_error_tolerance: int,
    predicate_hyperparameters: dict[str, float | int | str],

    # Execution flags for debugging
    benchmark_rl: bool,
    benchmark_model_based_rl: bool,
    benchmark_swmpo_rl: bool,

    # Shared results directory (assumed to exist). A subdir will be created
    results_dir: Path,
) -> Path:
    """Returns the directory with experiment results."""
    env_seed = "asd"

    # Get a hash of the source code
    src_files = list(Path(".").rglob("*.py")) + list(Path(".").rglob("*.sh"))
    src_files = [
        f
        for f in src_files
        if not any(
            exclude in str(f) for exclude in ["venv", "output_results"]
        )
    ]
    experiment_hash = "".join(f.read_text() for f in src_files)
    experiment_id = hashlib.md5(experiment_hash.encode()).hexdigest()

    date_str = str(datetime.datetime.now().isoformat())
    _output_dir = f"{env_id}_output_results_{date_str}_{experiment_id}"
    output_dir = (results_dir / _output_dir).with_suffix("")
    output_dir.mkdir()
    output_zip = output_dir.with_suffix(".zip")

    print(f"Output dir: {output_dir}")
    print(f"Output zip: {output_zip}")

    command_file = output_dir / "command.json"
    date_file = output_dir / "date.txt"
    plot_file = output_dir / "rewards.svg"
    tasks_costs_plot_file = output_dir / "task_costs.svg"
    rl_dir = output_dir / "rl_results"
    swmpo_dir = output_dir / "swmpo_results"
    model_based_rl_dir = output_dir / "model_based_rl_results"
    pretrained_partition_zip = output_dir / "pretrained_partition.zip"
    env_kwargs_path = output_dir / "env_kwargs.json"
    predicate_hyperparameters_json = output_dir / "predicate_hyperparameters.json"
    model_hyperparameters_path = output_dir/"model_hyperparameters.json"

    # Serialize code
    src_dir = output_dir / "src"
    src_dir.mkdir()
    subprocess.run(["mkdir", str(src_dir / "lib")])
    subprocess.run(["cp", "-r", "lib/swmpo", str(src_dir / "lib")])
    # We don't copy rl-trained-agents because it's huge
    subprocess.run([
        "rsync", "-a", "--exclude", "rl-trained-agents", "lib", str(src_dir)
    ])
    subprocess.run(["rm", "-rf", str(src_dir / "lib" / "rl-trained-agents")])
    subprocess.run(["cp", "-r", "swmpo_experiments", str(src_dir)])
    subprocess.run(["cp", "README.md", str(src_dir)])
    subprocess.run(["cp", "pyproject.toml", str(src_dir)])
    subprocess.run(["cp", "poetry.lock", str(src_dir)])

    # Log date
    with open(date_file, "w") as f:
        f.write(datetime.datetime.now().isoformat())

    # Log command used to run script
    with open(command_file, "wt") as fp:
        json.dump(sys.argv, fp, indent=2)

    jobs = list[subprocess.Popen]()

    # Write file with environment-specific keyword arguments
    with open(env_kwargs_path, "wt") as fp:
        json.dump(env_kwargs, fp, indent=2)

    # Write files with hyperparameters
    with open(predicate_hyperparameters_json, "wt") as fp:
        json.dump(predicate_hyperparameters, fp, indent=2)

    with open(model_hyperparameters_path, "wt") as fp:
        json.dump(model_hyperparameters, fp)

    # Write pretrained partition
    shutil.copy(partition_zip, pretrained_partition_zip)

    # Start a thread periodically plotting
    stop_event = threading.Event()  # Create a stop event locally
    plotting_interval_s = 60*5
    thread = threading.Thread(
        target=plot_rewards_until_signal,
        args=(
            stop_event,
            plotting_interval_s,
            swmpo_dir,
            rl_dir,
            model_based_rl_dir,
            src_dir,
            output_dir,
        ),
        daemon=True
    )
    thread.start()

    # Benchmark RL
    rl_dir.mkdir()
    if benchmark_rl:
        for experiment_i in range(rl_experiment_n):
            experiment_dir = rl_dir / str(experiment_i)
            j = async_submit_job([
                "python", str(src_dir / "swmpo_experiments" / "sequential_meta_learning" / "benchmark_rl.py"),
                "--output_dir", str(experiment_dir),
                "--train_timestep_n", str(rl_train_timestep_n),
                "--datapoint_n", str(rl_datapoint_n),
                "--plot_episode_n", str(rl_plot_episode_n),
                "--parallel_env_n", str(rl_parallel_env_n),
                "--eval_episode_n", str(rl_eval_episode_n),
                "--env_id", env_id,
                "--cuda_device", cuda_device,
                "--seed", str(experiment_i),
                "--env_seed", str(env_seed),
                "--env_kwargs", str(env_kwargs_path),
                "--stack_observations_n", str(stack_observations_n),
                "--session_train_steps", str(rl_session_train_steps),
            ])
            jobs.append(j)
            wait_for_jobs(worker_n, jobs)

    # Benchmark model-based RL
    model_based_rl_dir.mkdir()
    if benchmark_model_based_rl:
        for experiment_i in range(rl_experiment_n):
            experiment_dir = model_based_rl_dir / str(experiment_i)
            j = async_submit_job([
                "python", str(src_dir / "swmpo_experiments" / "sequential_meta_learning" / "benchmark_rl_model_based.py"),
                "--output_dir", str(experiment_dir),
                "--train_timestep_n", str(rl_train_timestep_n),
                "--datapoint_n", str(rl_datapoint_n),
                "--plot_episode_n", str(rl_plot_episode_n),
                "--parallel_env_n", str(rl_parallel_env_n),
                "--eval_episode_n", str(rl_eval_episode_n),
                "--env_id", env_id,
                "--cuda_device", cuda_device,
                "--seed", str(experiment_i),
                "--env_seed", str(env_seed),
                "--env_kwargs", str(env_kwargs_path),
                "--stack_observations_n", str(stack_observations_n),
                "--session_train_steps", str(rl_session_train_steps),
                "--model_dt", str(model_dt),
                "--model_episode_len", str(model_episode_len),
                "--model_based_rl_session_train_steps", str(model_based_rl_session_train_steps),
                "--model_based_rl_trajectory_buffer_size", str(model_based_rl_trajectory_buffer_size),
                "--model_hyperparameters_json", str(model_hyperparameters_path),
                "--data_collection_step_n", str(data_collection_step_n),
            ])
            jobs.append(j)
            wait_for_jobs(worker_n, jobs)

    # Benchmark model-based SWMPO RL
    swmpo_dir.mkdir()
    if benchmark_swmpo_rl:
        for experiment_i in range(rl_experiment_n):
            experiment_dir = swmpo_dir / str(experiment_i)
            args = [
                "python", str(src_dir / "swmpo_experiments" / "sequential_meta_learning" / "benchmark_rl_swmpo.py"),
                "--output_dir", str(experiment_dir),
                "--train_timestep_n", str(rl_train_timestep_n),
                "--datapoint_n", str(rl_datapoint_n),
                "--plot_episode_n", str(rl_plot_episode_n),
                "--parallel_env_n", str(rl_parallel_env_n),
                "--eval_episode_n", str(rl_eval_episode_n),
                "--env_id", env_id,
                "--cuda_device", cuda_device,
                "--seed", str(experiment_i),
                "--env_seed", str(env_seed),
                "--env_kwargs", str(env_kwargs_path),
                "--stack_observations_n", str(stack_observations_n),
                "--session_train_steps", str(rl_session_train_steps),
                "--model_dt", str(model_dt),
                "--model_episode_len", str(model_episode_len),
                "--model_based_rl_session_train_steps", str(model_based_rl_session_train_steps),
                "--model_based_rl_trajectory_buffer_size", str(model_based_rl_trajectory_buffer_size),
                "--data_collection_step_n", str(data_collection_step_n),
                "--partition_min_island_size", str(partition_min_island_size),
                "--partition_prunning_error_tolerance", str(partition_prunning_error_tolerance),
                "--predicate_hyperparameters_json", str(predicate_hyperparameters_json),
                "--pretrained_partition_zip", str(pretrained_partition_zip),
            ]
            j = async_submit_job(args)
            jobs.append(j)
            wait_for_jobs(worker_n, jobs)

    wait_for_jobs(0, jobs)

    stop_event.set()
    thread.join()

    print("Plotting")
    plot_rewards(
        swmpo_dir=swmpo_dir,
        rl_dir=rl_dir,
        model_based_rl_dir=model_based_rl_dir,
        output_dir=output_dir,
        src_dir=src_dir,
    )
    print("Done plotting")

    print("Pipeline complete!")
    subprocess.run(["zip", "-r", "-q", str(output_zip), str(output_dir)])
    print(f"Wrote {output_zip}")

    return output_dir


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='Experiment pipeline',
        description='Execute the full experiment pipeline.',
    )
    parser.add_argument(
        '--hyperparameter_json',
        type=Path,
        required=True,
        help='JSON file with hyperparameters.'
    )
    parser.add_argument(
        '--partition_zip',
        type=Path,
        required=True,
        default=None,
        help='Directory with pretrained local models.'
    )
    args = parser.parse_args()

    # Create results dir if it doesn't exist
    results_dir = Path("output_results")
    results_dir.mkdir(exist_ok=True)

    # Parse json
    with open(args.hyperparameter_json, "rt") as fp:
        hyperparameters = json.load(fp)

    main(
        **hyperparameters,
        partition_zip=args.partition_zip,
        results_dir=results_dir,
    )
