from gymnasium import spaces
import random
import numpy.typing as npt
import torch
import numpy as np
from pathlib import Path
from swmpo.transition import Transition
from stable_baselines3.common.vec_env import VecEnv
from gymnasium.wrappers import TimeLimit
from gymnasium.wrappers import TimeAwareObservation
from stable_baselines3.common.vec_env import DummyVecEnv
from stable_baselines3.common.vec_env import VecNormalize
from stable_baselines3 import SAC
from stable_baselines3.common.buffers import ReplayBuffer
from stable_baselines3.common.callbacks import EvalCallback
from stable_baselines3.common.logger import configure
from typing import Optional
from typing import Type
from typing import Any
from dataclasses import dataclass
import tempfile
import json
from swmpo.state_machine import StateMachine
from swmpo.state_machine import state_machine_model
from swmpo.world_models.world_model import WorldModel
from swmpo.state_machine import get_partition_induced_state_machine
from swmpo.state_machine import serialize_state_machine
from swmpo.state_machine import StatePartitionItem
from swmpo_experiments.sequential_meta_learning.model_based_training import RewardFunction
from swmpo_experiments.sequential_meta_learning.model_based_training import ModelEnv
from swmpo_experiments.sequential_meta_learning.model_based_training import collect_data
from swmpo_experiments.sequential_meta_learning.model_based_training import StatefulModel
from swmpo_experiments.sequential_meta_learning.model_based_training import evaluate_model_reward
from swmpo_experiments.sequential_meta_learning.model_based_training import evaluate_model_observations
from swmpo_experiments.sequential_meta_learning.policy_utils import get_model_copy
from swmpo_experiments.sequential_meta_learning.policy_utils import get_action_noise
from swmpo_experiments.visited_states_plotting import plot_visited_states


class FSMModel(StatefulModel):
    def __init__(
        self,
        state_machine: StateMachine,
        initial_observation: npt.NDArray,
        dt: float,
    ):
        self.state_machine = state_machine
        self.initial_observation = initial_observation
        self.dt = dt

        # State
        self.current_mode = 0
        self.current_observation = initial_observation

    def reset(self) -> dict[str, Any]:
        self.current_mode = 0
        self.current_observation = self.initial_observation
        info = dict(
            mode=self.current_mode,
        )
        return info

    def step(
        self,
        action: npt.NDArray,
    ) -> tuple[npt.NDArray, dict[str, Any]]:
        # Flatten current observation
        current_obs = self.current_observation.flatten()

        # Predict next flattened observation
        next_obs, next_mode = state_machine_model(
            state_machine=self.state_machine,
            state=torch.from_numpy(current_obs),
            action=torch.from_numpy(action),
            current_node=self.current_mode,
            dt=self.dt,
        )

        # Reshape observation
        # TODO parametrize bounds
        next_obs = np.clip(next_obs.detach().cpu().numpy(), -5.0, 5.0)
        next_obs = next_obs.reshape(self.initial_observation.shape)

        # Update state
        self.current_observation = next_obs
        self.current_mode = next_mode

        info = dict(
            mode=self.current_mode,
        )
        return next_obs, info


@dataclass
class ModeRecord:
    model_mode: int
    real_mode: int
    step_i: int


def evaluate_model_modes(
    model_env: VecEnv,
    env: VecEnv,
    policy: SAC,
    episode_n: int,
    output_dir: Path,
):
    data = list[list[ModeRecord]]()

    # Setup noise for exploration
    action_size = env.action_space.shape
    noises = [
        get_action_noise(action_size)
        for _ in range(env.num_envs)
    ]

    # Perform rollouts using the observations from the real environment
    obs = env.reset()
    model_obs = model_env.reset()
    step_i = [0 for _ in range(env.num_envs)]
    mode_sequences = [list[ModeRecord]() for _ in range(env.num_envs)]
    _episode_n = 0

    # Store initial modes (which should be equal for both
    # the model and the real env)
    for i in range(env.num_envs):
        # For some reason the VecEnv's reset_infos are all empty!
        continue
        real_mode = env.reset_infos[i]["mode"]
        model_mode = model_env.reset_infos[i]["mode"]
        record = ModeRecord(
            real_mode=real_mode,
            model_mode=model_mode,
            step_i=step_i[i],
        )
        mode_sequences[i].append(record)
    while _episode_n < episode_n:
        # Use observation from real environment to get action
        action, _ = policy.predict(obs, deterministic=False)

        for i in range(len(action)):
            action[i] = action[i] + noises[i]()

        # Step both env and model with the action
        obs, rewards, dones, infos = env.step(action)
        model_obs, model_rewards, _, model_infos = model_env.step(action)

        for i in range(env.num_envs):
            if dones[i]:
                step_i[i] = 0
                _episode_n += 1
                # Reset model env to guarantee modes are always compared
                # between the same timestep
                model_env.env_method("reset", indices=[i])
                env.env_method("reset", indices=[i])

                # Record mode sequence
                data.append(mode_sequences[i])
                mode_sequences[i] = list[ModeRecord]()
            else:
                step_i[i] += 1

                # Record observations
                real_mode = infos[i]["mode"]
                model_mode = model_infos[i]["mode"]
                record = ModeRecord(
                    real_mode=real_mode,
                    model_mode=model_mode,
                    step_i=step_i[i],
                )
                mode_sequences[i].append(record)

    # Save raw modes
    data_json = [
        [
            dict(
                real_mode=record.real_mode,
                model_mode=record.model_mode,
                step_i=record.step_i,
            )
            for record in sequence
        ]
        for sequence in data
    ]
    json_path = output_dir / "real_vs_model_modes.json"
    with open(json_path, "wt") as fp:
        json.dump(data_json, fp, indent=2)

    # Plot modes
    for i, sequence in enumerate(data):
        ground_truth = [record.real_mode for record in sequence]
        model_sequence = [record.model_mode for record in sequence]
        visited_states = dict(
            ground_truth=ground_truth,
            model=model_sequence,
        )
        available_indices = set(ground_truth + model_sequence)
        output_path = output_dir / f"episode_{i}.svg"
        plot_visited_states(
            visited_states=visited_states,
            available_indices=available_indices,
            output_path=output_path,
        )


def get_swmpo_model(
    current_task_data: list[list[Transition]],
    local_models: list[WorldModel],
    n_envs: int,
    reward_functions: list[RewardFunction],
    initial_observations: list[npt.NDArray],
    dt: float,
    seed: str,
    observation_spaces: list[spaces.Space],
    action_spaces: list[spaces.Space],
    episode_len: int,
    verbose: bool,
    is_stacked: bool,
    predicate_hyperparameters: dict[str, float | int | str],
    partition_min_island_size: int,
    partition_prunning_error_tolerance: float,
    log_dir: Path,
) -> VecEnv:
    _random = random.Random(seed)

    # Assemble partition using the local_models
    subsets = [list[Transition]() for _ in local_models]
    subset_is = list(range(len(local_models)))
    for trajectory in current_task_data:
        for transition in trajectory:
            i = min(
                subset_is,
                key=lambda i: local_models[i].get_raw_error(
                    transition=transition,
                    dt=dt,
                ),
            )
            subsets[i].append(transition)

    partition = [
        StatePartitionItem(
            local_model=local_model,
            subset=subset,
            hidden_sizes=[], # TODO: we're not using this anymore, remove it
        )
        for local_model, subset in zip(local_models, subsets)
    ]

    # Synthesize state machine
    state_machine = get_partition_induced_state_machine(
        partition=partition,
        predicate_hyperparameters=predicate_hyperparameters,
        seed=str(_random.random()),
    )

    # Log state machine
    state_machine_zip = log_dir / "state_machine.zip"
    serialize_state_machine(
        state_machine=state_machine,
        output_zip_path=state_machine_zip,
    )

    # Assemble model
    def env_maker(i: int):
        # Assemble model
        model = FSMModel(
            state_machine=state_machine,
            initial_observation=initial_observations[i],
            dt=dt,
        )
        env = ModelEnv(
            reward_function=reward_functions[i],
            model=model,
            initial_observation=initial_observations[i],
            observation_space=observation_spaces[i],
            action_space=action_spaces[i],
            is_stacked=is_stacked,
            episode_len=episode_len,
        )
        env = TimeLimit(
            env,
            max_episode_steps=episode_len,
        )
        # Parametrize whether to wrap in TimeAware
        # TerrainMass needs it, so I'm hardcoding it for now.
        env = TimeAwareObservation(
            env,
        )
        return env

    makers = [lambda: env_maker(i) for i in range(n_envs)]
    venv = DummyVecEnv(makers)
    return venv


def swmpo_learn(
    raw_train_env: VecEnv,
    vec_normalize: VecNormalize,
    model: SAC,
    data_collection_step_n: int,
    dt: float,
    seed: str,
    verbose: bool,
    model_based_rl_session_train_steps: int,
    model_based_rl_trajectory_buffer_size: int,
    eval_env: VecEnv,
    eval_freq: int,
    eval_episode_n: int,
    model_episode_len: int,
    log_dir: Path,
    is_stacked: bool,
    historical_task_data: list[list[Transition]],
    local_models: list[WorldModel],
    partition_min_island_size: int,
    partition_prunning_error_tolerance: float,
    predicate_hyperparameters: dict[str, float | int | str],
    replay_buffer_class: Optional[Type[ReplayBuffer]] = None,
) -> list[list[Transition]]:
    """Returns new transitions collected before training."""
    # Collect data
    data = collect_data(
        env=vec_normalize,
        model=model,
        step_n=data_collection_step_n,
    )

    # Sample past data
    _random = random.Random(seed)
    _historical_task_data = _random.sample(
        historical_task_data,
        min(model_based_rl_trajectory_buffer_size, len(historical_task_data)),
    )
    _data = _random.sample(
        data,
        min(model_based_rl_trajectory_buffer_size, len(data)),
    )

    # Synthesize model of environment
    observation_spaces = [
        unwrapped.observation_space
        for unwrapped in raw_train_env.get_attr("unwrapped")
    ]
    initial_observations = [
        unwrapped.reset()[1]["raw_observation"]
        for unwrapped in raw_train_env.get_attr("unwrapped")
    ]
    action_spaces = raw_train_env.get_attr("action_space")
    swmpo_log_dir = log_dir / "state_machine"
    swmpo_log_dir.mkdir()
    env_model = get_swmpo_model(
        current_task_data=_historical_task_data + _data,
        local_models=local_models,
        n_envs=raw_train_env.num_envs,
        reward_functions=raw_train_env.get_attr("reward_function"),
        initial_observations=initial_observations,
        dt=dt,
        seed=str(_random.random()),
        observation_spaces=observation_spaces,
        action_spaces=action_spaces,
        episode_len=model_episode_len,
        is_stacked=is_stacked,
        partition_min_island_size=partition_min_island_size,
        partition_prunning_error_tolerance=partition_prunning_error_tolerance,
        predicate_hyperparameters=predicate_hyperparameters,
        log_dir=swmpo_log_dir,
        verbose=verbose,
    )
    print(f"Wrote {swmpo_log_dir}")

    # Wrap environment with same normalization statistics
    with tempfile.TemporaryDirectory() as tdir:
        _dir = Path(tdir)
        _path = _dir / "vec_normalize"
        vec_normalize.save(_path)
        normalized_env_model = VecNormalize.load(_path, env_model)
        normalized_env_model.training = False

    # Evaluate environment reward tracking
    reward_eval_dir = log_dir / "reward_eval"
    reward_eval_dir.mkdir()
    evaluate_model_reward(
        model_env=normalized_env_model,
        env=vec_normalize,
        policy=model,
        output_dir=reward_eval_dir,
        episode_n=eval_episode_n,
    )
    print(f"Wrote {reward_eval_dir}")

    # Evaluate environment observations
    observations_eval_dir = log_dir / "observations_eval"
    observations_eval_dir.mkdir()
    evaluate_model_observations(
        model_env=normalized_env_model,
        env=vec_normalize,
        policy=model,
        output_dir=observations_eval_dir,
        episode_n=eval_episode_n,
    )
    normalized_env_model.reset()
    vec_normalize.reset()
    print(f"Wrote {observations_eval_dir}")

    # Evaluate model modes
    modes_eval_dir = log_dir / "modes_eval"
    modes_eval_dir.mkdir()
    evaluate_model_modes(
        model_env=normalized_env_model,
        env=vec_normalize,
        policy=model,
        output_dir=modes_eval_dir,
        episode_n=eval_episode_n,
    )
    normalized_env_model.reset()
    vec_normalize.reset()
    print(f"Wrote {modes_eval_dir}")

    # Model-based adaptation to new task
    log_path = log_dir / "sb3_log"
    eval_callback = EvalCallback(
        eval_env,
        eval_freq=eval_freq,
        n_eval_episodes=eval_episode_n,
        deterministic=True,
        render=False,
        verbose=True,
        log_path=str(log_path.resolve()),
    )

    # Copy policy
    new_model = get_model_copy(
        model=model,
        device="cpu",
    )
    new_model.set_env(normalized_env_model)
    new_logger = configure(str(log_path), ["stdout", "csv", "tensorboard"])
    new_model.set_logger(new_logger)

    # Iterate RL algorithm on the model environment
    new_model.learn(
        total_timesteps=model_based_rl_session_train_steps,
        callback=eval_callback,
        progress_bar=False,
        reset_num_timesteps=False,
    )
    model.set_parameters(new_model.get_parameters())

    return data
