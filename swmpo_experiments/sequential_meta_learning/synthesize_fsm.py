"""Given a policy, data from previous tasks and a task (MDP and initial state),
synthesize a task-specific FSM."""
from stable_baselines3.common.vec_env import VecEnv
from stable_baselines3 import SAC
from swmpo.state_machine import StateMachine


def synthesize_fsm(
    task_seed: str,
    env: VecEnv,
    model: SAC,
    episode_budget: int,
) -> StateMachine:
    """Given a policy, data from previous tasks and a task (MDP and
    initial state), synthesize a task-specific FSM by interacting with the
    environment."""
    # TODO
    pass
