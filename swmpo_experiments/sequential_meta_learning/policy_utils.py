import gymnasium
from gymnasium.utils.save_video import save_video
from stable_baselines3 import SAC
from stable_baselines3.common.logger import configure
from stable_baselines3.common.vec_env import VecEnv
from stable_baselines3.common.vec_env import VecNormalize
from stable_baselines3.common.noise import OrnsteinUhlenbeckActionNoise
from pathlib import Path
from typing import Optional
from typing import Type
from stable_baselines3.common.buffers import ReplayBuffer
import tempfile
from dataclasses import dataclass
import numpy as np


def is_policy_successful(
    model: SAC,
    task_seed: str,
    eval_env: VecEnv,
) -> bool:
    eval_env.env_method("reset_task", task_seed)
    obs = eval_env.reset()
    for _ in range(100000):
        action, _ = model.predict(obs, deterministic=True)
        obs, _, dones, infos = eval_env.step(action)
        success = any(
            info["success"]
            for info in infos
        )
        if success:
            return True
        if any(dones):
            break
    return False


@dataclass
class PolicyPath:
    policy_zip: Path
    normalized_venv_zip: Path
    step_n: int


def get_action_noise(action_dim: tuple[int, ...]) -> OrnsteinUhlenbeckActionNoise:
    action_noise = OrnsteinUhlenbeckActionNoise(
        mean=np.zeros(action_dim),
        sigma=0.5*np.ones(action_dim),
    )
    return action_noise


def get_model(
    train_env: VecEnv,
    cuda_device: str,
    seed: str,
    log_path: Path,
    replay_buffer_class: Optional[Type[ReplayBuffer]] = None,
) -> SAC:
    action_size = train_env.action_space.shape
    action_noise = get_action_noise(action_size)
    model = SAC(
        "MlpPolicy",
        train_env,
        verbose=1,
        # use_sde=False,
        seed=seed,
        # n_steps=2, # PPO
        #train_freq=(1, "episode"),
        #train_freq=(1, "step"),
        gradient_steps=-1,  # SAC
        device=cuda_device,
        replay_buffer_class=replay_buffer_class,
        learning_starts=0,
        use_sde=True,
        action_noise=action_noise,
    )
    new_logger = configure(str(log_path), ["stdout", "csv", "tensorboard"])
    model.set_logger(new_logger)
    return model


def get_timestamp() -> str:
    import datetime
    currentDT = datetime.datetime.now()
    return str(currentDT)


def get_model_copy(
    model: SAC,
    device: str,
) -> SAC:
    with tempfile.TemporaryDirectory() as tmpdirname:
        tdir = Path(tmpdirname)
        zip_path = tdir / "rl.zip"
        buffer_path = tdir / "my_buffer.pkl"
        model.save_replay_buffer(buffer_path)
        model.save(zip_path)
        new_model = SAC.load(
            zip_path,
            device=device,
        )
        new_model.load_replay_buffer(buffer_path)
    return new_model


def plot_policy(
    policy_path: PolicyPath,
    venv: VecEnv,
    task_seed: str,
    plot_env: gymnasium.Env,
    output_dir: Path,
):
    # Load policy and normalization statistics
    # This environment is only for normalization, it will not be step'd
    model = SAC.load(policy_path.policy_zip, device="cpu")

    normalized_env = VecNormalize.load(
        load_path=str(policy_path.normalized_venv_zip),
        venv=venv,
    )

    # Sample new task
    plot_env.reset_task(task_seed)
    obs, _ = plot_env.reset()
    step_starting_index = 0
    max_eval_steps = 1_000_000
    video_path = output_dir
    frames = list()
    frames.append(plot_env.render())
    for step_index in range(max_eval_steps):
        obs = normalized_env.normalize_obs(obs)
        action, _ = model.predict(obs, deterministic=True)
        obs, _, terminated, truncated, info = plot_env.step(action)
        frames.append(plot_env.render())
        if terminated or truncated:
            break

    if len(frames) > 0:
        episode_index = 0
        save_video(
            frames,
            str(video_path),
            fps=plot_env.metadata["render_fps"],
            step_starting_index=step_starting_index,
            episode_index=episode_index
        )


def serialize_and_plot(
    output_dir: Path,
    model: SAC,
    vec_normalize: VecNormalize,
    step_n: int,
    plot_env: gymnasium.Env,
    task_seed: str,
):
    """Output dir is assumed to exist."""
    # Serialize policy
    output_policy_path = output_dir/"RL_SB3_policy.zip"
    output_normalization_path = output_dir/"SB3_normalized_venv.zip"
    model.save(output_policy_path.with_suffix(''))
    vec_normalize.save(str(output_normalization_path))
    print(f"Wrote {output_policy_path}")
    print(f"Wrote {output_normalization_path}")

    # Plot policy
    task_policy_path = PolicyPath(
        policy_zip=output_policy_path,
        normalized_venv_zip=output_normalization_path,
        step_n=step_n,
    )

    if plot_env is not None:
        plot_policy(
            policy_path=task_policy_path,
            venv=vec_normalize,
            task_seed=task_seed,
            plot_env=plot_env,
            output_dir=output_dir,
        )
