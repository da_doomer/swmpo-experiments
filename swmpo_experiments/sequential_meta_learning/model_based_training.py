"""Provides an environment to perform model-based training."""
from collections.abc import Callable
from abc import ABC
from abc import abstractmethod
from pathlib import Path
from dataclasses import dataclass
import random
import tempfile
import json
import torch
import numpy as np
import pandas as pd
import seaborn as sns
import gymnasium as gym
from gymnasium import spaces
from gymnasium.wrappers import TimeLimit
from gymnasium.wrappers import TimeAwareObservation
import numpy.typing as npt
import matplotlib.pyplot as plt
from stable_baselines3.common.vec_env import VecEnv
from stable_baselines3.common.vec_env import DummyVecEnv
from stable_baselines3.common.vec_env import VecNormalize
from stable_baselines3 import SAC
from stable_baselines3.common.buffers import ReplayBuffer
from stable_baselines3.common.callbacks import EvalCallback
from stable_baselines3.common.logger import configure
from swmpo.transition import Transition
from swmpo.world_models.world_model import get_optimized_model
from swmpo.world_models.world_model import WorldModel
from swmpo_experiments.sequential_meta_learning.policy_utils import get_model_copy
from swmpo_experiments.sequential_meta_learning.policy_utils import get_action_noise
from typing import Optional
from typing import Type
from typing import Any


# RewardFunction(obs, action, next_obs, t_frac)
RewardFunction = Callable[
    [npt.NDArray, npt.NDArray, npt.NDArray, float],
    float,
]


class StatefulModel(ABC):
    """Step and reset also return an info dict."""
    @abstractmethod
    def step(
        self,
        action: npt.NDArray,
    ) -> tuple[npt.NDArray, dict[str, Any]]:
        pass

    @abstractmethod
    def reset(self) -> dict[str, Any]:
        pass


class ModelEnv(gym.Env[npt.NDArray, npt.NDArray]):
    def __init__(
        self,
        reward_function: RewardFunction,
        model: StatefulModel,
        initial_observation: npt.NDArray,
        observation_space: spaces.Space,
        action_space: spaces.Space,
        is_stacked: bool,
        episode_len: int,
    ):
        self.reward_function = reward_function
        self.model = model
        self.initial_observation = initial_observation
        self.observation_space = observation_space
        self.action_space = action_space
        self.is_stacked = is_stacked
        self.episode_len = episode_len

        # State
        self.current_observation = self.initial_observation
        self.step_i = 0
        self.model.reset()

    def reset(
        self,
        seed=None,
        options=None,
    ):
        super().reset(seed=seed)
        info = self.model.reset()
        self.current_observation = self.initial_observation
        self.step_i = 0
        return self.current_observation, info

    def step(self, action: npt.NDArray):
        self.step_i += 1
        next_observation, info = self.model.step(
            action,
        )
        if self.is_stacked:
            _obs_for_reward = self.current_observation[-1]
            _next_obs_for_reward = next_observation[-1]
        else:
            _obs_for_reward = self.current_observation
            _next_obs_for_reward = next_observation

        t_frac = self.step_i / self.episode_len
        reward = self.reward_function(
            _obs_for_reward,
            action,
            _next_obs_for_reward,
            t_frac,
        )
        terminated = False
        truncated = self.step_i > self.episode_len
        self.current_observation = next_observation
        return next_observation, reward, terminated, truncated, info


class NeuralModel(StatefulModel):
    def __init__(
        self,
        model: WorldModel,
        initial_observation: npt.NDArray,
        dt: float,
    ):
        """Assumes model wants flattened observations."""
        self.model = model
        self.initial_observation = initial_observation
        self.current_observation = initial_observation
        self.dt = dt

    def reset(self) -> dict[str, Any]:
        self.current_observation = self.initial_observation
        info = dict()
        return info

    def step(
        self,
        action: npt.NDArray,
    ) -> tuple[npt.NDArray, dict[str, Any]]:
        # Flatten current observation
        current_obs = self.current_observation.flatten()

        # Predict next flattened observation
        next_obs = self.model.get_prediction(
            action=torch.from_numpy(action),
            source_state=torch.from_numpy(current_obs),
            dt=self.dt,
        ).detach().numpy()

        # Clip observation
        # TODO: parametrize bounds
        next_obs = np.clip(next_obs, -5.0, 5.0)

        # Reshape observation
        next_obs = next_obs.reshape(self.initial_observation.shape)

        self.current_observation = next_obs
        info = dict()
        return next_obs, info


def get_neural_model(
    trajectories: list[list[Transition]],
    n_envs: int,
    reward_functions: list[RewardFunction],
    initial_observations: list[npt.NDArray],
    hyperparameters: dict[str, str | int | float],
    dt: float,
    seed: str,
    observation_spaces: list[spaces.Space],
    action_spaces: list[spaces.Space],
    episode_len: int,
    is_stacked: bool,
) -> VecEnv:

    nn = get_optimized_model(
        trajectories=trajectories,
        hyperparameters=hyperparameters,
        dt=dt,
        seed=seed,
    )

    # Synthesize model
    def env_maker(i: int):
        # Assemble model
        model = NeuralModel(
            model=nn,
            initial_observation=initial_observations[i],
            dt=dt,
        )
        env = ModelEnv(
            reward_function=reward_functions[i],
            model=model,
            initial_observation=initial_observations[i],
            observation_space=observation_spaces[i],
            action_space=action_spaces[i],
            is_stacked=is_stacked,
            episode_len=episode_len,
        )
        env = TimeLimit(
            env,
            max_episode_steps=episode_len,
        )
        # Parametrize whether to wrap in TimeAware
        # TerrainMass needs it, so I'm hardcoding it for now.
        env = TimeAwareObservation(
            env,
        )
        return env

    makers = [lambda: env_maker(i) for i in range(n_envs)]
    venv = DummyVecEnv(makers)
    return venv


def collect_data(
    env: VecNormalize,
    model: SAC,
    step_n: int,
) -> list[list[Transition]]:
    """Collects trajectories of flattened observations."""
    data = list[list[Transition]]()

    # We need to track the trajectories of each sub-env in the vecenv
    active_trajectories = [
        list[Transition]() for _ in range(env.num_envs)
    ]

    # Setup noise for exploration
    action_size = env.action_space.shape
    noises = [
        get_action_noise(action_size)
        for _ in range(env.num_envs)
    ]

    # Step every sub-env simultaneously
    obs = env.reset()
    infos = env.reset_infos
    initial_raw_observations = [
        unwrapped.reset()[1]["raw_observation"]
        for unwrapped in env.get_attr("unwrapped")
    ]
    for _ in range(step_n):
        action, _ = model.predict(obs, deterministic=False)

        for i in range(len(action)):
            action[i] = action[i] + noises[i]()

        new_obs, _, dones, new_infos = env.step(action)

        for i in range(env.num_envs):
            if dones[i]:
                # Store the trajectory and start a new one
                data.append(active_trajectories[i])
                active_trajectories[i] = list[Transition]()
            else:
                if "raw_observation" not in infos[i].keys():
                    # In the first reset, VecEnv doesn't propagate the
                    # infos correctly
                    source_obs = initial_raw_observations[i]
                else:
                    source_obs = infos[i]["raw_observation"]
                next_obs = new_infos[i]["raw_observation"]

                t = Transition(
                    source_state=torch.from_numpy(source_obs).flatten(),
                    action=torch.from_numpy(action[i]).flatten(),
                    next_state=torch.from_numpy(next_obs).flatten(),
                )
                active_trajectories[i].append(t)

        obs = new_obs
        infos = new_infos

    # Add all incomplete trajectories to the data
    for trajectory in active_trajectories:
        data.append(trajectory)

    return data


@dataclass
class StepRewardRecord:
    model_reward: float
    real_reward: float
    step_i: int


def evaluate_model_reward(
    model_env: VecEnv,
    env: VecEnv,
    policy: SAC,
    episode_n: int,
    output_dir: Path,
):
    data = list[StepRewardRecord]()

    # Setup noise for exploration
    action_size = env.action_space.shape
    noises = [
        get_action_noise(action_size)
        for _ in range(env.num_envs)
    ]

    # Perform rollouts using the observations from the real environment
    obs = model_env.reset()
    _episode_n = 0
    step_i = [0 for _ in range(env.num_envs)]
    while _episode_n < episode_n:
        # Use observation from real environment to get action
        action, _ = policy.predict(obs, deterministic=False)

        for i in range(len(action)):
            action[i] = action[i] + noises[i]()

        # Step both env and model with the action
        obs, rewards, dones, _ = env.step(action)
        _, model_rewards, _, _ = model_env.step(action)

        for i in range(env.num_envs):
            if dones[i]:
                step_i[i] = 0
                _episode_n += 1
                # Reset model env to guarantee rewards are always compared
                # between the same timestep
                model_env.env_method("reset", indices=[i])
                env.env_method("reset", indices=[i])
            else:
                step_i[i] += 1

                # Record rewards
                record = StepRewardRecord(
                    model_reward=model_rewards[i],
                    real_reward=rewards[i],
                    step_i=step_i[i],
                )
                data.append(record)

    # Plot reward-reward
    longform = []
    columns = [
        "model reward",
        "real reward",
        "model-induced reward - real reward",
        "episode timestep",
    ]
    longform = list[tuple[float, float]]()
    for i, datum in enumerate(data):
        reward_error = datum.model_reward - datum.real_reward
        row = (
            datum.model_reward,
            datum.real_reward,
            reward_error,
            datum.step_i,
        )
        longform.append(row)

    df = pd.DataFrame(
        longform,
        index=list(range(len(longform))),
        columns=columns,
    )

    # Reward-reward plot
    plt.figure()
    plot = sns.jointplot(
        x="real reward",
        y="model reward",
        kind="reg",
        data=df,
    )
    output_path = output_dir / "model_reward_diff.svg"
    plot.savefig(output_path)
    plt.close()

    # Timestep-reward error plot
    plt.figure()
    plot = sns.lineplot(
        x="episode timestep",
        y="model-induced reward - real reward",
        data=df,
    )
    output_path = output_dir / "model_reward_error.svg"
    plot.get_figure().savefig(output_path)
    plt.close()


@dataclass
class ObservationRecord:
    model_observation: list[float]
    real_observation: list[float]
    step_i: int


def evaluate_model_observations(
    model_env: VecEnv,
    env: VecEnv,
    policy: SAC,
    episode_n: int,
    output_dir: Path,
):
    data = list[ObservationRecord]()

    # Setup noise for exploration
    action_size = env.action_space.shape
    noises = [
        get_action_noise(action_size)
        for _ in range(env.num_envs)
    ]

    # Perform rollouts using the observations from the real environment
    obs = env.reset()
    model_obs = model_env.reset()
    _episode_n = 0
    step_i = [0 for _ in range(env.num_envs)]

    # Store initial observation (which should be equal for both
    # the model and the real env)
    for i in range(env.num_envs):
        record = ObservationRecord(
            model_observation=model_obs[i].tolist(),
            real_observation=obs[i].tolist(),
            step_i=step_i[i],
        )
        data.append(record)
    while _episode_n < episode_n:
        # Use observation from real environment to get action
        action, _ = policy.predict(obs, deterministic=False)

        for i in range(len(action)):
            action[i] = action[i] + noises[i]()

        # Step both env and model with the action
        obs, rewards, dones, _ = env.step(action)
        model_obs, model_rewards, _, _ = model_env.step(action)

        for i in range(env.num_envs):
            if dones[i]:
                step_i[i] = 0
                _episode_n += 1
                # Reset model env to guarantee rewards are always compared
                # between the same timestep
                model_env.env_method("reset", indices=[i])
                env.env_method("reset", indices=[i])
            else:
                step_i[i] += 1

                # Record observations
                record = ObservationRecord(
                    model_observation=model_obs[i].tolist(),
                    real_observation=obs[i].tolist(),
                    step_i=step_i[i],
                )
                data.append(record)

    # Save raw observations
    data_json = [
        dict(
            model_observation=record.model_observation,
            real_observation=record.real_observation,
            step_i=record.step_i,
        )
        for record in data
    ]
    json_path = output_dir / "real_vs model_observations.json"
    with open(json_path, "wt") as fp:
        json.dump(data_json, fp, indent=2)

    # Plot reward-reward
    longform = []
    columns = [
        "observation error",
        "episode timestep",
    ]
    longform = list[tuple[float, float]]()
    for i, datum in enumerate(data):
        observation_delta = torch.tensor(datum.model_observation) - torch.tensor(datum.real_observation)
        observation_error = observation_delta.norm().item()
        row = (
            observation_error,
            datum.step_i,
        )
        longform.append(row)

    df = pd.DataFrame(
        longform,
        index=list(range(len(longform))),
        columns=columns,
    )

    # Timestep-obs error plot
    plt.figure()
    plot = sns.lineplot(
        x="episode timestep",
        y="observation error",
        data=df,
    )
    output_path = output_dir / "observations_error.svg"
    plot.get_figure().savefig(output_path)
    plt.close()


def model_based_learn(
    raw_train_env: VecEnv,
    vec_normalize: VecNormalize,
    model: SAC,
    data_collection_step_n: int,
    dt: float,
    seed: str,
    device: str,
    verbose: bool,
    model_based_rl_session_train_steps: int,
    model_based_rl_trajectory_buffer_size: int,
    model_hyperparameters: dict[str, str | int | float],
    eval_env: VecEnv,
    eval_freq: int,
    eval_episode_n: int,
    model_episode_len: int,
    log_dir: Path,
    is_stacked: bool,
    historical_task_data: list[list[Transition]],
    replay_buffer_class: Optional[Type[ReplayBuffer]] = None,
) -> list[list[Transition]]:
    """Returns new transitions collected before training."""
    # Collect data
    data = collect_data(
        env=vec_normalize,
        model=model,
        step_n=data_collection_step_n,
    )

    # Sample past data
    _random = random.Random(seed)
    _historical_task_data = _random.sample(
        historical_task_data,
        min(model_based_rl_trajectory_buffer_size, len(historical_task_data)),
    )
    _data = _random.sample(
        data,
        min(model_based_rl_trajectory_buffer_size, len(data)),
    )

    # Synthesize model of environment
    observation_spaces = [
        unwrapped.observation_space
        for unwrapped in raw_train_env.get_attr("unwrapped")
    ]
    initial_observations = [
        unwrapped.reset()[1]["raw_observation"]
        for unwrapped in raw_train_env.get_attr("unwrapped")
    ]
    action_spaces = raw_train_env.get_attr("action_space")
    env_model = get_neural_model(
        trajectories=_historical_task_data + _data,
        n_envs=raw_train_env.num_envs,
        reward_functions=raw_train_env.get_attr("reward_function"),
        initial_observations=initial_observations,
        hyperparameters=model_hyperparameters,
        dt=dt,
        seed=str(_random.random()),
        observation_spaces=observation_spaces,
        action_spaces=action_spaces,
        episode_len=model_episode_len,
        is_stacked=is_stacked,
    )

    # Wrap environment with same normalization statistics
    with tempfile.TemporaryDirectory() as tdir:
        _dir = Path(tdir)
        _path = _dir / "vec_normalize"
        vec_normalize.save(_path)
        normalized_env_model = VecNormalize.load(_path, env_model)
        normalized_env_model.training = False

    # Evaluate environment reward tracking
    reward_eval_dir = log_dir / "reward_eval"
    reward_eval_dir.mkdir()
    evaluate_model_reward(
        model_env=normalized_env_model,
        env=vec_normalize,
        policy=model,
        output_dir=reward_eval_dir,
        episode_n=eval_episode_n,
    )
    normalized_env_model.reset()
    vec_normalize.reset()
    print(f"Wrote {reward_eval_dir}")

    # Evaluate environment observations
    observations_eval_dir = log_dir / "observations_eval"
    observations_eval_dir.mkdir()
    evaluate_model_observations(
        model_env=normalized_env_model,
        env=vec_normalize,
        policy=model,
        output_dir=observations_eval_dir,
        episode_n=eval_episode_n,
    )
    normalized_env_model.reset()
    vec_normalize.reset()
    print(f"Wrote {observations_eval_dir}")

    # Model-based adaptation to new task
    log_path = log_dir / "sb3_log"
    eval_callback = EvalCallback(
        eval_env,
        eval_freq=eval_freq,
        n_eval_episodes=eval_episode_n,
        deterministic=True,
        render=False,
        verbose=True,
        log_path=str(log_path.resolve()),
    )

    # Copy policy
    new_model = get_model_copy(
        model=model,
        device=device,
    )
    new_model.set_env(normalized_env_model)
    new_logger = configure(str(log_path), ["stdout", "csv", "tensorboard"])
    new_model.set_logger(new_logger)

    # Iterate RL algorithm on the model environment
    new_model.learn(
        total_timesteps=model_based_rl_session_train_steps,
        callback=eval_callback,
        progress_bar=False,
        reset_num_timesteps=False,
    )
    model.set_parameters(new_model.get_parameters())

    return data
