"""Train an RL agent in a sequential multi-task setting. After a task
is completed, another task is sampled.

There is a model-based adaptation phase at the start of each task.

The following is assumed of the environment:
- exposes an `set_task(seed: str)` method.
- returns a `success` key in the `info` dict in every step.

This script runs the SWMPO version of model-based training.
"""
from typing import Any
from pathlib import Path
import argparse
import random
import json

from swmpo.transition import Transition
from swmpo.partition import deserialize_partition

import swmpo_experiments.autonomous_driving_utils.ground_truth_wrapper
import swmpo_experiments.terrain_mass_utils.ground_truth_wrapper
import swmpo_experiments.salamander_utils.ground_truth_wrapper
import swmpo_experiments.bipedal_walker_hardcore_utils.ground_truth_wrapper

from swmpo_experiments.sequential_meta_learning.policy_utils import get_model
from swmpo_experiments.sequential_meta_learning.policy_utils import get_timestamp
from swmpo_experiments.sequential_meta_learning.policy_utils import is_policy_successful
from swmpo_experiments.sequential_meta_learning.policy_utils import serialize_and_plot
from swmpo_experiments.sequential_meta_learning.swmpo_training import swmpo_learn

from stable_baselines3.common.env_util import make_vec_env
from stable_baselines3.common.vec_env import VecNormalize
from stable_baselines3.common.callbacks import EvalCallback

import gymnasium
from gymnasium.wrappers import FrameStack
from gymnasium.wrappers import TimeAwareObservation

import terrain_mass
import autonomous_car_verification
import salamander_env
import bipedal_walker_hardcore_modes

import torch


# Avoid pytorch from doing threading. This is so that the script doesn't
# take over the computer's resources. You can remove these lines if not running
# on a lab computer.
torch.set_num_threads(1)


def perform_experiment(
    env_id: str,
    output_dir: Path,
    train_timestep_n: int,
    datapoint_n: int,
    plot_episode_n: int,
    seed: str,
    parallel_env_n: int,
    eval_episode_n: int,
    cuda_device: str,
    env_seed: str,
    env_kwargs: dict[str, Any],
    stack_observations_n: int,
    session_train_steps: int,
    model_dt: float,
    model_episode_len: int,
    model_based_rl_session_train_steps: int,
    model_based_rl_trajectory_buffer_size: int,
    data_collection_step_n: int,
    partition_min_island_size: int,
    partition_prunning_error_tolerance: float,
    predicate_hyperparameters: dict[str, float | int | str],
    pretrained_partition_zip: Path,
):
    total_steps = 0
    current_task_steps = 0
    current_task_data = list[list[Transition]]()
    steps_required_per_task = list[int]()

    # Deserialize pre-trained partition to initialize
    # local models and historical data
    partition = deserialize_partition(pretrained_partition_zip)
    local_models = [item.local_model for item in partition]

    def wrapper(env):
        if stack_observations_n > 0:
            env = FrameStack(
                env,
                stack_observations_n,
            )
        # TODO: Parametrize whether to wrap in TimeAware
        # TerrainMass needs it, so I'm hardcoding it for now.
        env = TimeAwareObservation(
            env,
        )
        return env

    # Create train and evaluation environments
    def make_env():
        _random = random.Random(env_seed)
        venv = make_vec_env(
            env_id=env_id,
            n_envs=parallel_env_n,
            seed=int.from_bytes(_random.randbytes(3), 'big', signed=False),
            wrapper_class=wrapper,
            env_kwargs=dict(
                render_mode=None,
                **env_kwargs,
            )
        )
        return venv
    train_env = make_env()
    eval_env = make_env()

    # Create plotting environment
    if plot_episode_n > 0:
        plot_env = gymnasium.make(
            env_id,
            render_mode="rgb_array",
            **env_kwargs,
        )
        plot_env = wrapper(plot_env)
    else:
        plot_env = None

    # Decide how often to evaluate the policy
    eval_freq = max(1, train_timestep_n//parallel_env_n//datapoint_n)

    # Benchmark RL on environment
    is_stacked = stack_observations_n > 0

    # Wrap environments
    raw_train_env = train_env
    train_env = VecNormalize(
        venv=train_env,
        training=True,
        norm_obs=True,
        norm_reward=False,
        clip_reward=1.0,
    )
    eval_env = VecNormalize(
        venv=eval_env,
        training=False,
        norm_obs=True,
        norm_reward=False,
        clip_reward=1.0,
    )

    # Optimize policy
    _random = random.Random(seed)
    log_path = output_dir / f"real_rl_{get_timestamp()}"
    replay_buffer_class = None
    model = get_model(
        train_env=train_env,
        cuda_device=cuda_device,
        seed=int.from_bytes(_random.randbytes(3), 'big', signed=False),
        replay_buffer_class=replay_buffer_class,
        log_path=log_path,
    )

    # Setup callback
    eval_callback = EvalCallback(
        eval_env,
        eval_freq=eval_freq,
        n_eval_episodes=eval_episode_n,
        deterministic=True,
        render=False,
        verbose=True,
    )

    # Sequential meta-learning loop: train until a task is solved, then
    # sample new task, iterate.
    task_seed = str(_random.random())
    train_env.env_method("reset_task", task_seed)
    eval_env.env_method("reset_task", task_seed)
    while total_steps < train_timestep_n:
        print(f"Real world steps {total_steps}/{train_timestep_n}")
        print("Real world RL round")
        total_steps += session_train_steps
        current_task_steps += session_train_steps
        model.learn(
            total_timesteps=session_train_steps,
            callback=eval_callback,
            progress_bar=False,
            reset_num_timesteps=False,
        )

        # TODO: add newest data from replay buffer to current_task_data

        # Do model-based RL if appropriate
        do_model_based_this_round = all((
            model_based_rl_session_train_steps > 0,
            len(local_models) > 0,
        ))
        if do_model_based_this_round:
            print("SWMPO model-based RL round")
            model_based_log_dir = output_dir / f"model_based_rl_{get_timestamp()}"
            model_based_log_dir.mkdir()
            train_env.env_method("reset_task", task_seed)
            eval_env.env_method("reset_task", task_seed)
            total_steps += data_collection_step_n
            current_task_steps += data_collection_step_n
            new_data = swmpo_learn(
                local_models=local_models,
                raw_train_env=raw_train_env,
                vec_normalize=train_env,
                model=model,
                data_collection_step_n=data_collection_step_n,
                dt=model_dt,
                seed=str(_random.random()),
                model_based_rl_session_train_steps=model_based_rl_session_train_steps,
                model_based_rl_trajectory_buffer_size=model_based_rl_trajectory_buffer_size,
                eval_env=eval_env,
                eval_freq=eval_freq,
                eval_episode_n=eval_episode_n,
                log_dir=model_based_log_dir,
                model_episode_len=model_episode_len,
                replay_buffer_class=replay_buffer_class,
                is_stacked=is_stacked,
                historical_task_data=current_task_data,
                partition_min_island_size=partition_min_island_size,
                partition_prunning_error_tolerance=partition_prunning_error_tolerance,
                predicate_hyperparameters=predicate_hyperparameters,
                verbose=True,
            )
            model.set_env(train_env)

            # Update task data
            current_task_data.extend(new_data)

    # Store cost to solve task
    steps_required_per_task.append(current_task_steps)
    current_task_steps = 0

    # Serialize policy
    task_output_dir = output_dir / f"task_result_{get_timestamp()}"
    task_output_dir.mkdir()
    serialize_and_plot(
        output_dir=task_output_dir,
        model=model,
        vec_normalize=train_env,
        step_n=total_steps,
        plot_env=plot_env,
        task_seed=task_seed,
    )

    # Serialize task costs
    steps_required_per_task_path = output_dir/"steps_required_per_task.json"
    with open(steps_required_per_task_path, "wt") as fp:
        json.dump(steps_required_per_task, fp, indent=2)
    print(f"Wrote {steps_required_per_task_path}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='RL benchmark',
        description='Run RL',
    )
    parser.add_argument(
        '--output_dir',
        type=Path,
        required=True,
        help='Non-existing directory to write output files'
    )
    parser.add_argument(
        '--train_timestep_n',
        type=int,
        required=True,
        help='Number of gradient descent iterations for the state machine optimization process'
    )
    parser.add_argument(
        '--env_id',
        type=str,
        required=True,
        help='ID of the gymnasium environment.'
    )
    parser.add_argument(
        '--datapoint_n',
        type=int,
        required=True,
        help=(
            "Number of datapoints. A datapoint is a measurement of values for"
            " a particular training timestep. A value of `x` means that the"
            " policy will be evaluated every `train_timestep_n//x` timesteps."
        ),
    )
    parser.add_argument(
        '--plot_episode_n',
        type=int,
        required=True,
        help='Number of episodes to plot after training.'
    )
    parser.add_argument(
        '--parallel_env_n',
        type=int,
        required=True,
        help='Number of parallel environments in each worker.'
    )
    parser.add_argument(
        '--eval_episode_n',
        type=int,
        required=True,
        help='Number of episodes to evaluate policies on during training.',
    )
    parser.add_argument(
        '--cuda_device',
        type=str,
        required=True,
        help='CUDA device for SGD optimization',
    )
    parser.add_argument(
        '--seed',
        type=str,
        required=True,
        help='Random number generator seed (for the RL algorithm)'
    )
    parser.add_argument(
        '--env_seed',
        type=str,
        required=True,
        help='Environment `make(seed)` seed.'
    )
    parser.add_argument(
        '--env_kwargs',
        type=Path,
        required=True,
        help='JSON path with environment constructor kwargs.'
    )
    parser.add_argument(
        '--stack_observations_n',
        type=int,
        required=True,
        help='Number of past observations to stack.'
    )
    _ = parser.add_argument(
        '--model_dt',
        type=float,
        required=True,
        help='Integration constant for the dynamical system.',
    )
    _ = parser.add_argument(
        '--model_episode_len',
        type=int,
        required=True,
        help=(
            "Episode length in the environment model."
        )
    )
    _ = parser.add_argument(
        '--session_train_steps',
        type=int,
        required=True,
        help=(
            "Continuous steps in each RL training session."
        )
    )
    _ = parser.add_argument(
        '--model_based_rl_session_train_steps',
        type=int,
        required=True,
        help=( "Continuous steps in each model-based RL training session."
        )
    )
    _ = parser.add_argument(
        '--model_based_rl_trajectory_buffer_size',
        type=int,
        required=True,
        help=(
            "Maximum number of trajectories to use in model training."
        )
    )
    _ = parser.add_argument(
        '--data_collection_step_n',
        type=int,
        required=True,
        help=(
            "Number of steps to collect data before a model-based session."
        )
    )
    _ = parser.add_argument(
        '--partition_min_island_size',
        type=int,
        required=True,
        help='Minimum size for mode "islands" in the initial partitions (smaller islands get prunned).',
    )
    _ = parser.add_argument(
        '--partition_prunning_error_tolerance',
        type=float,
        required=True,
        help='Minimum size for mode "islands" in the initial partitions (smaller islands get prunned).',
    )
    _ = parser.add_argument(
        '--predicate_hyperparameters_json',
        type=Path,
        required=True,
        help='JSON with predicate synthesis hyperparameters'
    )
    parser.add_argument(
        '--pretrained_partition_zip',
        type=Path,
        required=False,
        default=None,
        help='Directory with pretrained local models (for debugging).'
    )
    args = parser.parse_args()
    output_dir = args.output_dir
    output_dir.mkdir()

    with open(args.env_kwargs, "rt") as fp:
        env_kwargs = json.load(fp)

    with open(args.predicate_hyperparameters_json, "rt") as fp:
        predicate_hyperparameters = json.load(fp)

    perform_experiment(
        env_id=args.env_id,
        output_dir=output_dir,
        seed=args.seed,
        train_timestep_n=args.train_timestep_n,
        datapoint_n=args.datapoint_n,
        plot_episode_n=args.plot_episode_n,
        parallel_env_n=args.parallel_env_n,
        eval_episode_n=args.eval_episode_n,
        cuda_device=args.cuda_device,
        env_seed=args.env_seed,
        env_kwargs=env_kwargs,
        stack_observations_n=args.stack_observations_n,
        session_train_steps=args.session_train_steps,
        model_dt=args.model_dt,
        model_episode_len=args.model_episode_len,
        model_based_rl_session_train_steps=args.model_based_rl_session_train_steps,
        model_based_rl_trajectory_buffer_size=args.model_based_rl_trajectory_buffer_size,
        data_collection_step_n=args.data_collection_step_n,
        partition_min_island_size=args.partition_min_island_size,
        partition_prunning_error_tolerance=args.partition_prunning_error_tolerance,
        predicate_hyperparameters=predicate_hyperparameters,
        pretrained_partition_zip=args.pretrained_partition_zip,
    )
