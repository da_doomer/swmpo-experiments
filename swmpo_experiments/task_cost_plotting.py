"""Plotting utilities for the example scripts."""
from pathlib import Path
import seaborn as sns
import pandas as pd
import json
import argparse


def plot_task_costs(
    algorithm_to_jsons: dict[str, list[Path]],
    output_path: Path,
):
    # Plot each CSV
    longform = []
    columns = [
        "experiment_id",
        "task #",
        "task cost",
        "algorithm",
    ]
    experiment_id = 0
    for algorithm_id, jsons in algorithm_to_jsons.items():
        for json_path in jsons:
            experiment_id += 1

            x = list[int]()
            y = list[float]()

            print(f"Reading {json_path}")
            with open(json_path, "rt") as fp:
                data = json.load(fp)

            for task_i, cost in enumerate(data):
                xi = task_i
                yi = cost

                try:
                    xi = int(xi)
                    yi = float(yi)
                    x.append(xi)
                    y.append(yi)
                except:
                    continue

            for task_i, cost in zip(x, y):
                row = (
                    experiment_id,
                    task_i,
                    cost,
                    algorithm_id
                )
                longform.append(row)

    # Plot the responses for different events and regions
    df = pd.DataFrame(
        longform,
        index=list(range(len(longform))),
        columns=columns,
    )
    plot = sns.lineplot(
        x="task #",
        y="task cost",
        hue="algorithm",
        data=df,
    )

    fig = plot.get_figure()
    fig.savefig(output_path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog='Reward plotting',
        description=(
            'Plot the evaluation metrics in the given CSVs from'
            ' StableBaselines3 training.'
        ),
    )
    parser.add_argument(
        '--swmpo_jsons',
        type=Path,
        nargs='*',
        help='CSV files from StableBaselines3 training for SWMPO.',
    )
    parser.add_argument(
        '--biased_swmpo_jsons',
        type=Path,
        nargs='*',
        help='CSV files from StableBaselines3 training for Biased SWMPO.',
    )
    parser.add_argument(
        '--rl_jsons',
        type=Path,
        nargs='*',
        help='CSV files from StableBaselines3 training for RL.',
    )
    parser.add_argument(
        '--stacked_rl_jsons',
        type=Path,
        nargs='*',
        help='CSV files from StableBaselines3 training for RL.',
    )
    parser.add_argument(
        '--swmpo_ground_truth_jsons',
        type=Path,
        nargs='*',
        help='CSV files from StableBaselines3 training for Biased SWMPO Ground Truth.',
    )
    parser.add_argument(
        '--biased_swmpo_ground_truth_jsons',
        type=Path,
        nargs='*',
        help='CSV files from StableBaselines3 training for Biased SWMPO Ground Truth.',
    )
    parser.add_argument(
        '--model_based_rl_jsons',
        type=Path,
        nargs='*',
        help='CSV files from StableBaselines3 training for Biased SWMPO Ground Truth.',
    )
    parser.add_argument(
        '--output_path',
        type=Path,
        required=True,
        help='PNG or SVG file to write with the plot.'
    )
    args = parser.parse_args()

    # Organize the CSVS for the different algorithms
    algorithm_to_jsons = dict(
        rl=args.rl_jsons if args.rl_jsons is not None else list(),
        swmpo=args.swmpo_jsons if args.swmpo_jsons is not None else list(),
        biased_swmpo=args.biased_swmpo_jsons if args.biased_swmpo_jsons is not None else list(),
        swmpo_ground_truth=args.swmpo_ground_truth_jsons if args.swmpo_ground_truth_jsons is not None else list(),
        biased_swmpo_ground_truth=args.biased_swmpo_ground_truth_jsons if args.biased_swmpo_ground_truth_jsons is not None else list(),
        stacked_rl=args.stacked_rl_jsons if args.stacked_rl_jsons is not None else list(),
        model_based_rl=args.model_based_rl_jsons if args.model_based_rl_jsons is not None else list(),
    )

    plot_task_costs(
        algorithm_to_jsons=algorithm_to_jsons,
        output_path=args.output_path,
    )
