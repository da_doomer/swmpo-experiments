# SWMPO experiments

This repository contains code to run SWMPO on some example environments.

## SWMPO

The SWMPO repository is [gitlab.com/da_doomer/swmpo](https://gitlab.com/da_doomer/swmpo).

## Environments

- [gitlab.com/da_doomer/terrain-mass](https://gitlab.com/da_doomer/terrain-mass).
- [gitlab.com/da_doomer/autonomous_car_verification](https://gitlab.com/da_doomer/autonomous-verification). Adapted from [github.com/keyshor/autonomous_car_verification](https://github.com/keyshor/autonomous_car_verification).
- [gitlab.com/da_doomer/salamander-env](https://gitlab.com/da_doomer/salamander-env).
- [gitlab.com/da_doomer/bipedal-walker-hardcore-modes](https://gitlab.com/da_doomer/bipedal-walker-hardcore-modes).

## How to run

Clone and change directory to the repository. Then create and activate a python
virtual environment and execute one of the scripts to verify you can run the
code. I recommend using Poetry for dependency management.

In bash, this would look something like this:

```sh
git clone --recurse-submodules https://gitlab.com/da_doomer/swmpo-experiments.git
cd swmpo-experiments
python -m venv venv
source venv/bin/activate
python -m pip install poetry
poetry install
pip install -e lib/swmpo
pip install -e lib/terrain-mass
pip install -e lib/autonomous-verification
pip install -e lib/salamander-env
pip install -e lib/bipedal-walker-hardcore-modes
# Required because of SSM baseline library has a setup.py with dependencies:
pip install numpy cython
pip install git+https://github.com/lindermanlab/ssm

# Modeling pipeline
python swmpo_experiments/pipeline.py --hyperparameter_json swmpo_experiments/salamander.json

# RL pipeline
# Find any global partition ZIP in the output directory of the modeling
# pipeline and run (the partition contains the neural primitives)
python swmpo_experiments/sequential_meta_learning/main.py --hyperparameter_json swmpo_experiments/salamander.json --partition_zip path/to/partition.zip
```

The salamander script requires [Webots](https://en.wikipedia.org/wiki/Webots) and
[`xvfb`](https://en.wikipedia.org/wiki/Xvfb). Fffpmeg should be in your path.
